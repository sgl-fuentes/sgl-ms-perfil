package com.sgl.ms.perfil.domain.repository.catalog;

import com.sgl.ms.perfil.domain.dto.catalog.DocumentType;
import com.sgl.ms.perfil.domain.dto.catalog.Group;
import com.sgl.ms.perfil.domain.dto.catalog.IdentityDocumentType;

import java.util.List;
import java.util.Optional;

public interface IdentityDocumentTypeRepository {
    List<IdentityDocumentType> getAll();
    IdentityDocumentType save(IdentityDocumentType identityDocumentType);
    void delete (int identityDocumentTypeId);
    Optional<IdentityDocumentType> getById(int id);
}
