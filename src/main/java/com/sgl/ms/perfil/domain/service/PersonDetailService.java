package com.sgl.ms.perfil.domain.service;

import com.sgl.ms.perfil.domain.dto.PersonDetail;
import com.sgl.ms.perfil.domain.dto.request.PersonDetailRequest;
import com.sgl.ms.perfil.domain.repository.PersonDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonDetailService {
    @Autowired
    private PersonDetailRepository personDetailRepository;

    public Optional<List<PersonDetail>> getPersonDetailByPersonaId(int personDetailId) {
        return personDetailRepository.getPersonDetailByPersonaId(personDetailId);
    }

    public List<PersonDetail> saveAll(List<PersonDetail> personDetails) {
        return personDetailRepository.saveAll(personDetails);
    }

    public boolean delete(int personDetailId) {
        return personDetailRepository.getById(personDetailId).map(personDetail -> {
            personDetailRepository.delete(personDetailId);
            return true;
        }).orElse(false);
    }
}
