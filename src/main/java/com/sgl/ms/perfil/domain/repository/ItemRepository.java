package com.sgl.ms.perfil.domain.repository;

import com.sgl.ms.perfil.domain.dto.Item;
import com.sgl.ms.perfil.domain.dto.Option;

public interface ItemRepository {
    Item save(Item item);
}
