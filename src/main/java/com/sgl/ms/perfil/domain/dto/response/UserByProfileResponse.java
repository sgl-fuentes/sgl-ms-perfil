package com.sgl.ms.perfil.domain.dto.response;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonValue;
import com.sgl.ms.perfil.domain.dto.UserProfile;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserByProfileResponse {
    //private int id;
    private List<UserProfile> users;
}
