package com.sgl.ms.perfil.domain.dto;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Profile {
    private Integer id;
    private int userCreationId;
    private LocalDateTime creationDate;
    private int userModificationId;
    private LocalDateTime modificationDate;
    private String code;
    private String description;
    private String shortDescription;
    private String programCode;
    private String filterCode;
    //private List<UserProfile> users;
}
