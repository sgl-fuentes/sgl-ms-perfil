package com.sgl.ms.perfil.domain.dto.profile.option;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CProfile {
    private int id;
    private int userCreationId;
    private LocalDateTime creationDate;
    private int userModificationId;
    private LocalDateTime modificationDate;
    private String code;
    private String description;
    private String shortDescription;
    private String programCode;
    private String filterCode;
}
