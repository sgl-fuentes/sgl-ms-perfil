package com.sgl.ms.perfil.domain.dto.response;

import java.util.List;

public class OptionResponse
{
    private int id;
    private String label;
    private String icon;
    private String path;
    private Boolean selected = false;
    private Boolean indeterminate = false;
    private int order;
    private List<ItemResponse> children;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Boolean getIndeterminate() {
        return indeterminate;
    }

    public void setIndeterminate(Boolean indeterminate) {
        this.indeterminate = indeterminate;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<ItemResponse> getChildren() {
        return children;
    }

    public void setChildren(List<ItemResponse> children) {
        this.children = children;
    }
}
