package com.sgl.ms.perfil.domain.service;

import com.sgl.ms.perfil.domain.dto.Person;
import com.sgl.ms.perfil.domain.dto.Profile;
import com.sgl.ms.perfil.domain.dto.User;
import com.sgl.ms.perfil.domain.dto.response.UserProfileResponse;
import com.sgl.ms.perfil.domain.repository.PersonRepository;
import com.sgl.ms.perfil.domain.repository.ProfileRepository;
import com.sgl.ms.perfil.domain.repository.UserProfileRepository;
import com.sgl.ms.perfil.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserProfileRepository userProfileRepository;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private PersonRepository personRepository;

    public List<User> getAll() {
        List<User> users = userRepository.getAll();
        for(User us : users){
            List<UserProfileResponse> userProfileResponses = userProfileRepository.findByUsuarioId(us.getId());
            if(userProfileResponses.size() == 0){
                us.setProfile(null);
            }
            else {
                Profile profile = profileRepository.getById(userProfileResponses.get(0).getProfileId()).get();
                us.setProfile(profile);
            }
            if(us.getPersonId() != null){
                Person person = personRepository.getById(us.getPersonId()).get();
                if(person != null){
                    us.setPerson(person);
                }
            }
            else{
                us.setPerson(null);
            }

        }
        return users;
    }
    public List<User> getUserWithoutProfile() {
        return userRepository.getUserWithoutProfile();
    }
    public Optional<User> getById(int idProfile) {
        return userRepository.getById(idProfile);
    }
    public Optional<User> getByCode(String code) {
        return userRepository.getByCodigo(code);
    }

    public Optional<User> getById(Integer id) {
        return userRepository.getById(id);
    }
    public Optional<User> getByEmail(String email) {
        return userRepository.getByEmail(email);
    }
    public Optional<User> getByCodeAndPassword(String code, String password) {
        return userRepository.getByCodigoAndPassword(code, password);
    }

    public User save(User user) {
        user.setNovo(true);
        LocalDateTime now = LocalDateTime.now();
        if(user.getCreationDate() == null) {
            user.setCreationDate(now);
        }
        if(user.getModificationDate() == null) {
            user.setModificationDate(now);
        }
        return userRepository.save(user);
    }

    public User update(User user) {
        Optional<User> updatedUser = userRepository.getById(user.getId());
        User newUser = updatedUser.get();
        newUser.setNovo(user.getNovo());
        newUser.setPassword(user.getPassword());
        if(user.getStatusId() != null){
            newUser.setStatusId(user.getStatusId());
        }
        if(user.getCode() != null){
            newUser.setCode(user.getCode());
        }
        if(user.getLastName() != null){
            newUser.setLastName(user.getLastName());
        }
        if(user.getFirstName() != null){
            newUser.setFirstName(user.getFirstName());
        }
        if(user.getSecondName() != null){
            newUser.setSecondName(user.getSecondName());
        }
        if(user.getSecondLastName() != null){
            newUser.setSecondLastName(user.getSecondLastName());
        }
        if(user.getEmail() != null){
            newUser.setEmail(user.getEmail());
        }
        return userRepository.update(newUser);
    }
}
