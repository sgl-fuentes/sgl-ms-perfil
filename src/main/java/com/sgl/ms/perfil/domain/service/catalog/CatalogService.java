package com.sgl.ms.perfil.domain.service.catalog;

import com.sgl.ms.perfil.domain.dto.catalog.*;
import com.sgl.ms.perfil.domain.repository.catalog.DocumentTypeRepository;
import com.sgl.ms.perfil.domain.repository.catalog.GroupRepository;
import com.sgl.ms.perfil.domain.repository.catalog.IdentityDocumentTypeRepository;
import com.sgl.ms.perfil.domain.repository.catalog.WorkPositionRepository;
import com.sgl.ms.perfil.persistence.mapper.catalog.CatalogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CatalogService {
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private IdentityDocumentTypeRepository identityDocumentTypeRepository;
    @Autowired
    private WorkPositionRepository workPositionRepository;
    @Autowired
    private DocumentTypeRepository documentTypeRepository;
    @Autowired
    private CatalogMapper catalogMapper;
    public List<?> getAllCatalog(String catalog) {
        switch(catalog) {
            case "Group":
                return groupRepository.getAll();
            case "WorkPosition":
                return workPositionRepository.getAll();
            case "IdentityDocumentType":
                return identityDocumentTypeRepository.getAll();
            case "DocumentType":
                return documentTypeRepository.getAll();
            default:
                return new ArrayList<>();
        }
    }

    public DocumentType saveDocumentType(Catalog catalog) {
        DocumentType documentType = catalogMapper.toDocumentType(catalog);
        return documentTypeRepository.save(documentType);
    }
    public Group saveGroup(Catalog catalog) {
        Group group = catalogMapper.toGroup(catalog);
        return groupRepository.save(group);
    }
    public IdentityDocumentType saveIdentityDocumentType(Catalog catalog) {
        IdentityDocumentType identityDocumentType1 = catalogMapper.toIdentityDocumentType(catalog);
        return identityDocumentTypeRepository.save(identityDocumentType1);
    }
    public WorkPosition saveWorkPosition(Catalog catalog) {
        WorkPosition workPosition = catalogMapper.toWorkPosition(catalog);
        return workPositionRepository.save(workPosition);
    }
    public boolean deleteDocumentType(int id) {
        return documentTypeRepository.getById(id).map(i -> {
            documentTypeRepository.delete(id);
            return true;
        }).orElse(false);
    }
    public boolean deleteGroup(int id) {
        return groupRepository.getById(id).map(i -> {
            groupRepository.delete(id);
            return true;
        }).orElse(false);
    }
    public boolean deleteIdentityDocumentType(int id) {
        return identityDocumentTypeRepository.getById(id).map(i -> {
            identityDocumentTypeRepository.delete(id);
            return true;
        }).orElse(false);
    }
    public boolean deleteWorkPosition(int id) {
        return workPositionRepository.getById(id).map(i -> {
            workPositionRepository.delete(id);
            return true;
        }).orElse(false);
    }

    public Optional<DocumentType> getDocumentTypeId(int id){
        return documentTypeRepository.getById(id);
    }
    public Optional<Group> getGroupId(int id){
        return groupRepository.getById(id);
    }
    public Optional<IdentityDocumentType> getIdentityDocumentTypeId(int id){
        return identityDocumentTypeRepository.getById(id);
    }
    public Optional<WorkPosition> getWorkPositionId(int id){
        return workPositionRepository.getById(id);
    }
}
