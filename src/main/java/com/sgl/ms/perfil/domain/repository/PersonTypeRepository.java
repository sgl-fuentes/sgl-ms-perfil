package com.sgl.ms.perfil.domain.repository;

import com.sgl.ms.perfil.domain.dto.PersonType;

import java.util.List;
import java.util.Optional;

public interface PersonTypeRepository {
    List<PersonType> getAll();
    Optional<PersonType> getById(int personTypeId);
    PersonType save(PersonType personType);
    boolean delete(int personTypeId);
}
