package com.sgl.ms.perfil.domain.service;

import com.sgl.ms.perfil.domain.dto.ProfileUser;
import com.sgl.ms.perfil.domain.dto.response.UserProfileResponse;
import com.sgl.ms.perfil.domain.repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserProfileService {
    @Autowired
    private UserProfileRepository userProfileRepository;

    public ProfileUser save(ProfileUser profileUser) {
        return userProfileRepository.save(profileUser);
    }

    public List<UserProfileResponse> findByUsuarioId(int idUsuario) {
        return userProfileRepository.findByUsuarioId(idUsuario);
    }
}
