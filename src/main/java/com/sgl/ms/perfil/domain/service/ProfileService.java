package com.sgl.ms.perfil.domain.service;

import com.sgl.ms.perfil.domain.dto.Profile;
import com.sgl.ms.perfil.domain.dto.ProfileMenu;
import com.sgl.ms.perfil.domain.dto.ProfileUser;
import com.sgl.ms.perfil.domain.dto.UserProfile;
import com.sgl.ms.perfil.domain.dto.profile.control.CControl;
import com.sgl.ms.perfil.domain.dto.profile.control.CControlProfile;
import com.sgl.ms.perfil.domain.dto.response.*;
import com.sgl.ms.perfil.domain.repository.MenuRepository;
import com.sgl.ms.perfil.domain.repository.ProfileMenuRepository;
import com.sgl.ms.perfil.domain.repository.ProfileRepository;
import com.sgl.ms.perfil.domain.repository.UserProfileRepository;
import com.sgl.ms.perfil.domain.service.profile.control.ControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProfileService {
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private UserProfileRepository userProfileRepository;
    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private ProfileMenuRepository profileMenuRepository;
    @Autowired
    private ControlService controlService;

    public List<Profile> getAll() {
        return profileRepository.getAll();
    }

    public List<Profile> getByDescription(String description) {
        return profileRepository.getByDescription(description);
    }

    public Optional<Profile> getById(int idProfile) {
        return profileRepository.getById(idProfile);
    }

    public Profile save(Profile profile) {
        LocalDateTime now = LocalDateTime.now();
        if(profile.getCreationDate() == null) {
            profile.setCreationDate(now);
        }
        if(profile.getModificationDate() == null) {
            profile.setModificationDate(now);
        }

        Profile preparedProfile = profileRepository.save(profile);

        if(profile.getId() == null) {
            List<MenuResponse> menuResponses = menuRepository.findByLevel(1);

            for(MenuResponse mr : menuResponses) {
                for(OptionResponse or : mr.getChildren()) {
                    for(ItemResponse ir : or.getChildren()) {
                        ProfileMenu profileMenu = new ProfileMenu();
                        profileMenu.setUserCreationId(1);
                        profileMenu.setCreationDate(LocalDateTime.now());
                        profileMenu.setUserModificationId(1);
                        profileMenu.setModificationDate(LocalDateTime.now());
                        profileMenu.setMenuId(ir.getId());
                        profileMenu.setProfileId(preparedProfile.getId());
                        profileMenuRepository.save(profileMenu);
                    }
                    ProfileMenu profileMenu = new ProfileMenu();
                    profileMenu.setUserCreationId(1);
                    profileMenu.setCreationDate(LocalDateTime.now());
                    profileMenu.setUserModificationId(1);
                    profileMenu.setModificationDate(LocalDateTime.now());
                    profileMenu.setMenuId(or.getId());
                    profileMenu.setProfileId(preparedProfile.getId());
                    profileMenuRepository.save(profileMenu);
                }
                ProfileMenu profileMenu = new ProfileMenu();
                profileMenu.setUserCreationId(1);
                profileMenu.setCreationDate(LocalDateTime.now());
                profileMenu.setUserModificationId(1);
                profileMenu.setModificationDate(LocalDateTime.now());
                profileMenu.setMenuId(mr.getId());
                profileMenu.setProfileId(preparedProfile.getId());
                profileMenuRepository.save(profileMenu);
            }

            /*
            List<CControl> controls = controlService.getAll();
            List<CControlProfile> cControlProfiles = new ArrayList<>();

            for(CControl control : controls) {
                CControlProfile cControlProfile = new CControlProfile();
                cControlProfile.setUserCreationId(1);
                cControlProfile.setCreationDate(LocalDateTime.now());
                cControlProfile.setUserModificationId(1);
                cControlProfile.setModificationDate(LocalDateTime.now());
                cControlProfile.setCcontrolId(control.getId());
                cControlProfile.setProfileId(preparedProfile.getId());
                cControlProfile.setFlag(false);
                cControlProfiles.add(cControlProfile);
            }

            controlService.saveAll(cControlProfiles);
            */
        }

        return preparedProfile;
    }


    public ProfileUser saveProfileUser(ProfileUser profileUser) {
        LocalDateTime now = LocalDateTime.now();
        if(profileUser.getCreationDate() == null) {
            profileUser.setCreationDate(now);
        }
        if(profileUser.getModificationDate() == null) {
            profileUser.setModificationDate(now);
        }
        if(profileUser.getUserCreationId() == 0 ) {
            profileUser.setUserCreationId(1);
        }
        if(profileUser.getUserModificationId() == 0) {
            profileUser.setUserModificationId(1);
        }
        return userProfileRepository.save(profileUser);
    }

    public List<ProfileUser> saveListProfileUser(List<ProfileUser> profileUsers) {
        LocalDateTime now = LocalDateTime.now();
        List<ProfileUser> responseProfileUser = new ArrayList();
        for(ProfileUser pu : profileUsers)
        {
            if(pu.getCreationDate() == null) {
                pu.setCreationDate(now);
            }
            if(pu.getModificationDate() == null) {
                pu.setModificationDate(now);
            }
            if(pu.getUserCreationId() == 0 ) {
                pu.setUserCreationId(1);
            }
            if(pu.getUserModificationId() == 0) {
                pu.setUserModificationId(1);
            }
            responseProfileUser.add(userProfileRepository.save(pu));
        }
        return responseProfileUser;
    }
    public Optional<UserByProfileResponse> getUserByProfileId(int idProfile) {
        return profileRepository.getUserByProfileId(idProfile);
    }

    public ProfileUser findById(int userProfileId) {
        return userProfileRepository.findById(userProfileId);
    }
    public boolean deleteById(int userId, int profileId) {
        UserProfileResponse userProfileResponse = getByUserIdAndProfileId(userId, profileId);
        if(userProfileResponse == null) {
            return false;
        }
        else {
            userProfileRepository.deleteById(userProfileResponse.getId());
            return true;
        }
    }
    public UserProfileResponse getByUserIdAndProfileId(int userId, int profileId) {
        return userProfileRepository.getByUserIdAndProfileId(userId, profileId);
    }
}
