package com.sgl.ms.perfil.domain.service;

import com.sgl.ms.perfil.domain.dto.ProfileMenu;
import com.sgl.ms.perfil.domain.repository.ProfileMenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileMenuService {
    @Autowired
    private ProfileMenuRepository profileMenuRepository;

    public ProfileMenu save(ProfileMenu profileMenu) {
        return profileMenuRepository.save(profileMenu);
    }
}
