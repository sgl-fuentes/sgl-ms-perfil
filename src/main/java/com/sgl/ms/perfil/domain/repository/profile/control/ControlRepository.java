package com.sgl.ms.perfil.domain.repository.profile.control;

import com.sgl.ms.perfil.domain.dto.profile.control.CControl;
import com.sgl.ms.perfil.domain.dto.profile.control.CControlProfile;

import java.util.List;
import java.util.Optional;

public interface ControlRepository {
    List<CControl> getAll();
    Optional<List<CControl>> getByOptionId(int id);
}
