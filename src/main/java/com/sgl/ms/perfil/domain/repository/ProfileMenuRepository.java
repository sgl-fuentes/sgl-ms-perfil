package com.sgl.ms.perfil.domain.repository;

import com.sgl.ms.perfil.domain.dto.ProfileMenu;

public interface ProfileMenuRepository {

    ProfileMenu save(ProfileMenu profileMenu);

}
