package com.sgl.ms.perfil.domain.dto;

import com.sgl.ms.perfil.domain.dto.catalog.DocumentType;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonDetail {
    private int id;
    private int userCreationId;
    private LocalDateTime creationDate;
    private int userModificationId;
    private LocalDateTime modificationDate;
    private Integer personId;
    private Integer documentTypeId;
    private String image;
    //INI Nuevos Campos
    private LocalDate dueDate;
    private Integer detailTypeId;
    private Integer companyId;
    private String account;
    private String cciAccount;
    private Integer statusId;
    //FIN Nuevos Campos
    private Person person;
    private DocumentType documentType;
    private Status status;
}
