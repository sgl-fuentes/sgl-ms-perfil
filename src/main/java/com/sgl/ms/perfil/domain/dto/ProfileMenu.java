package com.sgl.ms.perfil.domain.dto;

import lombok.*;

import java.time.LocalDateTime;
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfileMenu {
    private int id;
    private int userCreationId;
    private LocalDateTime creationDate;
    private int userModificationId;
    private LocalDateTime modificationDate;
    private int menuId;
    private int profileId;
    private Boolean selected;
    private Boolean indeterminate;
}
