package com.sgl.ms.perfil.domain.repository;

import com.sgl.ms.perfil.domain.dto.ProfileUser;
import com.sgl.ms.perfil.domain.dto.Purchase;
import com.sgl.ms.perfil.domain.dto.UserProfile;
import com.sgl.ms.perfil.domain.dto.response.UserProfileResponse;
import com.sgl.ms.perfil.persistence.entity.UsuarioPerfil;

import java.util.List;

public interface UserProfileRepository {
    ProfileUser save(ProfileUser profileUser);
    List<UserProfileResponse> findByUsuarioId(int idUsuario);
    void deleteById (int id);
    ProfileUser findById(int userProfileId);
    UserProfileResponse getByUserIdAndProfileId(int userId, int profileId);
}
