package com.sgl.ms.perfil.domain.dto.request;

import com.sgl.ms.perfil.domain.dto.Person;
import com.sgl.ms.perfil.domain.dto.catalog.DocumentType;
import lombok.*;

import java.time.LocalDateTime;
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonDetailRequest {
    private int id;
    private int userCreationId;
    private LocalDateTime creationDate;
    private int userModificationId;
    private LocalDateTime modificationDate;
    private Integer personId;
    private Integer documentTypeId;
    private Person person;
    private DocumentType documentType;
    private String image;

}
