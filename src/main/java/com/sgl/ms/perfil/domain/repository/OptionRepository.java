package com.sgl.ms.perfil.domain.repository;

import com.sgl.ms.perfil.domain.dto.Option;

public interface OptionRepository {
    Option save(Option option);
}
