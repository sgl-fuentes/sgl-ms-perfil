package com.sgl.ms.perfil.domain.service.profile.control;

import com.sgl.ms.perfil.domain.dto.profile.control.ControlType;
import com.sgl.ms.perfil.domain.repository.profile.control.ControlTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ControlTypeService {
    @Autowired
    private ControlTypeRepository controlTypeRepository;

    public List<ControlType> getAll() {
        return controlTypeRepository.getAll();
    }
    public Optional<ControlType> getControlTypeById(int id) {
        return controlTypeRepository.getControlTypeById(id);
    }

    public ControlType save(ControlType controlType) {
        return controlTypeRepository.save(controlType);
    }
}
