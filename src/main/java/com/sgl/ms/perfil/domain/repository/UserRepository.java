package com.sgl.ms.perfil.domain.repository;

import com.sgl.ms.perfil.domain.dto.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<User> getAll();
    List<User> getUserWithoutProfile();
    Optional<User> getById(int idProfile);
    Optional<User> getByCodigo(String codigo);
    Optional<User> getByEmail(String email);
    Optional<User> getByCodigoAndPassword(String codigo, String password);
    User save(User user);
    User update(User user);
}
