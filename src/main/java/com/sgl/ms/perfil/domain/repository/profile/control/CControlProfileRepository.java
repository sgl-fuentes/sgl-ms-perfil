package com.sgl.ms.perfil.domain.repository.profile.control;

import com.sgl.ms.perfil.domain.dto.profile.control.CControlProfile;

import java.util.List;
import java.util.Optional;

public interface CControlProfileRepository {
    List<CControlProfile> getAll();
    CControlProfile save(CControlProfile cControlProfile);
    Optional<List<CControlProfile>> getByPerfilId(int id);
    Optional<List<CControlProfile>> getByControlIdAndPerfilId(int controlId, int profileId);
    Integer deleteByProfileId(int profileId);
    Integer deleteById(int id);
}