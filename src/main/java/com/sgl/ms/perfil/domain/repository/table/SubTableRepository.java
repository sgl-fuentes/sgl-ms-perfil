package com.sgl.ms.perfil.domain.repository.table;

import com.sgl.ms.perfil.domain.dto.table.SubTable;
import com.sgl.ms.perfil.domain.dto.table.Table;

import java.util.List;
import java.util.Optional;

public interface SubTableRepository {
    Optional<List<SubTable>> getByOptionId(int optionId);
}
