package com.sgl.ms.perfil.domain.service.profile.option;

import com.sgl.ms.perfil.domain.dto.profile.option.CProfile;
import com.sgl.ms.perfil.domain.repository.profile.option.CProfileRepository;
import com.sgl.ms.perfil.persistence.crud.perfil.custom.CustomPerfil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CProfileService {
    @Autowired
    private CProfileRepository cProfileRepository;

    public List<CProfile> getAll() {
        return cProfileRepository.getAll();
    }

    public List<CProfile> findByFirstLevel() {
        return cProfileRepository.findByFirstLevel();
    }

    public List<CustomPerfil> getCustomPerfil() {
        return cProfileRepository.getCustomPerfil();
    }
}
