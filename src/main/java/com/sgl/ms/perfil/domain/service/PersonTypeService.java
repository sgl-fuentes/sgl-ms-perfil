package com.sgl.ms.perfil.domain.service;

import com.sgl.ms.perfil.domain.dto.PersonType;
import com.sgl.ms.perfil.domain.repository.PersonTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonTypeService {
    @Autowired
    private PersonTypeRepository personTypeRepository;

    public List<PersonType> getAll() {
        return personTypeRepository.getAll();
    }
    public Optional<PersonType> getPersonTypeById(int personTypeId) {
        return personTypeRepository.getById(personTypeId);
    }

    public PersonType save(PersonType personType) {
        return personTypeRepository.save(personType);
    }

    public boolean delete(int personTypeId) {
        return getPersonTypeById(personTypeId).map(personType -> {
            personTypeRepository.delete(personTypeId);
            return true;
        }).orElse(false);
    }
}
