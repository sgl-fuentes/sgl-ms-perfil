package com.sgl.ms.perfil.domain.dto.catalog;

import lombok.*;

import java.time.LocalDateTime;
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Catalog {
    private int id;
    private int userCreationId;
    private LocalDateTime creationDate;
    private int userModificationId;
    private LocalDateTime modificationDate;
    private String code;
    private String description;
    private String shortDescription;
    private String programCode;
    private String filterCode;
}
