package com.sgl.ms.perfil.domain.repository;

import com.sgl.ms.perfil.domain.dto.Profile;
import com.sgl.ms.perfil.domain.dto.response.UserByProfileResponse;

import java.util.List;
import java.util.Optional;

public interface ProfileRepository {
    List<Profile> getAll();
    List<Profile> getByDescription(String description);
    Optional<Profile> getById(int idProfile);
    Profile save(Profile profile);
    Optional<UserByProfileResponse> getUserByProfileId(int idProfile);
}
