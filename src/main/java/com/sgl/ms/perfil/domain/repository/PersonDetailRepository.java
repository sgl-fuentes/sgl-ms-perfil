package com.sgl.ms.perfil.domain.repository;

import com.sgl.ms.perfil.domain.dto.Person;
import com.sgl.ms.perfil.domain.dto.PersonDetail;
import com.sgl.ms.perfil.domain.dto.request.PersonDetailRequest;

import java.util.List;
import java.util.Optional;

public interface PersonDetailRepository {
    List<PersonDetail> getAll();
    PersonDetail save(PersonDetail personDetail);
    Optional<List<PersonDetail>> getPersonDetailByPersonaId(int personDetailId);
    void delete (int personDetailId);
    Optional<PersonDetail> getById(int id);
    List<PersonDetail> saveAll(List<PersonDetail> personDetails);
}
