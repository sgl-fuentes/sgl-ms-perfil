package com.sgl.ms.perfil.domain.repository.profile.option;

import com.sgl.ms.perfil.domain.dto.profile.option.CProfile;
import com.sgl.ms.perfil.persistence.crud.perfil.custom.CustomPerfil;

import java.util.List;

public interface CProfileRepository {
    List<CProfile> getAll();
    List<CProfile> findByFirstLevel();
    List<CustomPerfil> getCustomPerfil();
}
