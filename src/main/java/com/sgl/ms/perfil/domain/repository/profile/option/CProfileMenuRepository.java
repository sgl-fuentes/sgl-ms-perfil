package com.sgl.ms.perfil.domain.repository.profile.option;

import com.sgl.ms.perfil.domain.dto.Product;
import com.sgl.ms.perfil.domain.dto.profile.option.CProfileMenu;
import com.sgl.ms.perfil.persistence.entity.perfil.opcion.CPerfilBarra;

import java.util.List;

public interface CProfileMenuRepository {
    List<CProfileMenu> getByProfileId(int profileId);
    CProfileMenu findByPerfilIdAndOpcionId(int idPerfil, int idOpcion);
    CProfileMenu save(CProfileMenu cProfileMenu);
    Integer deleteByProfileId(int profileId);
}
