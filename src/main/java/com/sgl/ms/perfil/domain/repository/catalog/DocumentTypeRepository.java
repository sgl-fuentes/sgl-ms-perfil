package com.sgl.ms.perfil.domain.repository.catalog;

import com.sgl.ms.perfil.domain.dto.Menu;
import com.sgl.ms.perfil.domain.dto.catalog.DocumentType;
import com.sgl.ms.perfil.domain.dto.catalog.WorkPosition;

import java.util.List;
import java.util.Optional;

public interface DocumentTypeRepository {
    List<DocumentType> getAll();
    DocumentType save(DocumentType documentType);
    void delete (int documentTypeTypeId);
    Optional<DocumentType> getById(int id);
}
