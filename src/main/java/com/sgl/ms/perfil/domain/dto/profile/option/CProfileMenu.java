package com.sgl.ms.perfil.domain.dto.profile.option;

import lombok.*;

import java.time.LocalDateTime;
public class CProfileMenu {
    private int id;
    private int userCreationId;
    private LocalDateTime creationDate;
    private int userModificationId;
    private LocalDateTime modificationDate;
    private int menuId;
    private int profileId;
    private Boolean selected;
    private Boolean indeterminate;
    private CProfile customProfile;
    private CMenu customMenu;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserCreationId() {
        return userCreationId;
    }

    public void setUserCreationId(int userCreationId) {
        this.userCreationId = userCreationId;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public int getUserModificationId() {
        return userModificationId;
    }

    public void setUserModificationId(int userModificationId) {
        this.userModificationId = userModificationId;
    }

    public LocalDateTime getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDateTime modificationDate) {
        this.modificationDate = modificationDate;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Boolean getIndeterminate() {
        return indeterminate;
    }

    public void setIndeterminate(Boolean indeterminate) {
        this.indeterminate = indeterminate;
    }

    public CProfile getCustomProfile() {
        return customProfile;
    }

    public void setCustomProfile(CProfile customProfile) {
        this.customProfile = customProfile;
    }

    public CMenu getCustomMenu() {
        return customMenu;
    }

    public void setCustomMenu(CMenu customMenu) {
        this.customMenu = customMenu;
    }
}
