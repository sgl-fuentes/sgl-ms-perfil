package com.sgl.ms.perfil.domain.service.profile.control;

import com.sgl.ms.perfil.domain.dto.profile.control.CControl;
import com.sgl.ms.perfil.domain.dto.profile.control.CControlProfile;
import com.sgl.ms.perfil.domain.dto.response.CControlProfileResponse;
import com.sgl.ms.perfil.domain.repository.profile.control.CControlProfileRepository;
import com.sgl.ms.perfil.domain.repository.profile.control.ControlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ControlService {
    @Autowired
    private ControlRepository controlRepository;
    @Autowired
    private CControlProfileRepository cControlProfileRepository;

    public List<CControl> getAll() {
        return controlRepository.getAll();
    }

    public List<CControlProfile> getCControlProfileByProfileId(int profileId) {
        return cControlProfileRepository.getByPerfilId(profileId).get();
    }

    List<CControlProfile> getByControlIdAndPerfilId(int controlId, int profileId){
        return cControlProfileRepository.getByControlIdAndPerfilId(controlId, profileId).get();
    }

    public List<CControlProfileResponse> saveAll(List<CControlProfile> cControlProfiles) {
        List<CControlProfileResponse> cControlProfileResponses = new ArrayList<>();
        for(CControlProfile cc : cControlProfiles){
            CControlProfileResponse cControlProfileResponse = new CControlProfileResponse();
            CControlProfile cControlProfileResult = cControlProfileRepository.save(cc);
            cControlProfileResponse.setId(cControlProfileResult.getId());
            cControlProfileResponse.setUserCreationId(cControlProfileResult.getUserCreationId());
            cControlProfileResponse.setCreationDate(cControlProfileResult.getCreationDate());
            cControlProfileResponse.setUserModificationId(cControlProfileResult.getUserModificationId());
            cControlProfileResponse.setModificationDate(cControlProfileResult.getModificationDate());
            cControlProfileResponse.setProfileId(cControlProfileResult.getProfileId());
            cControlProfileResponse.setCcontrolId(cControlProfileResult.getCcontrolId());
            cControlProfileResponse.setFlag(cControlProfileResult.getFlag());
            cControlProfileResponses.add(cControlProfileResponse);
        }
        return cControlProfileResponses;
    }
    @Transactional
    public Integer deleteByProfileId(int profileId) {
        return cControlProfileRepository.deleteByProfileId(profileId);
    }
    public Integer deleteById(int id) {
        return cControlProfileRepository.deleteById(id);
    }
}
