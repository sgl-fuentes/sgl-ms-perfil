package com.sgl.ms.perfil.domain.dto;

import com.sgl.ms.perfil.domain.dto.catalog.Group;
import com.sgl.ms.perfil.domain.dto.catalog.WorkPosition;
import lombok.*;
import com.sgl.ms.perfil.domain.dto.catalog.IdentityDocumentType;

import java.time.LocalDateTime;
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private Integer id;
    private Integer userCreationId;
    private LocalDateTime creationDate;
    private Integer userModificationId;
    private LocalDateTime modificationDate;
    private Integer identityDocumentTypeId;
    private String identityDocument;
    private String lastName;
    private String secondLastName;
    private String firstName;
    private String secondName;
    private String email;
    private String phone;
    private Integer jobId;
    private Integer groupId;
    private LocalDateTime hireDate;
    private Integer regionId;
    private Integer statusId;
    private Integer costCenterId;
    private Double salary;
    private Integer personTypeId;
    private String code;
    private IdentityDocumentType identityDocumentType;
    private WorkPosition job;
    private Group group;
    private CostCenter costCenter;
    private Area region;
    private Status status;
    private PersonType personType;
}
