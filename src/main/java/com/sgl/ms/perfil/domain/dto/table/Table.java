package com.sgl.ms.perfil.domain.dto.table;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Table {
    //private int id;
    //private int userCreationId;
    //private LocalDateTime creationDate;
    //private int userModificationId;
    //private LocalDateTime modificationDate;
    private String label;
    //private String description;
    //private String shortDescription;
    //private String programCode;
    //private String filterCode;
    //private Integer optionId;
    //private String icon;
    //private String uri;
    //private Integer fatherId;
    private Integer order;
    //private Integer tableTypeId;
    //private Integer level;
    private List<SubTable> children;
}
