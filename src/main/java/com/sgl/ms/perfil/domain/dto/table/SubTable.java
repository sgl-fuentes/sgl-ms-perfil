package com.sgl.ms.perfil.domain.dto.table;

import lombok.*;

import java.time.LocalDateTime;
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubTable {
    //private int id;
    //private int userCreationId;
    //private LocalDateTime creationDate;
    //private int userModificationId;
    //private LocalDateTime modificationDate;
    private String code;
    private String label;
    //private String shortDescription;
    //private String programCode;
    //private String filterCode;
    //private Integer optionId;
    //private String icon;
    //private String uri;
    //private Integer fatherId;
    private Integer order;
    //private Integer tableTypeId;
    //private Integer level;
}
