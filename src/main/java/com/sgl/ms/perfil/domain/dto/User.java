package com.sgl.ms.perfil.domain.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Integer id;
    private Integer userCreationId;
    private LocalDateTime creationDate;
    private Integer userModificationId;
    private LocalDateTime modificationDate;
    private String code;
    private String password;
    private String lastName;
    private String secondLastName;
    private String firstName;
    private String secondName;
    private String email;
    private Integer personId;
    private Boolean novo;
    private Integer statusId;
    private Status status;
    private Profile profile;
    private Person person;
}
