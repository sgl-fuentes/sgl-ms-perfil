package com.sgl.ms.perfil.domain.service.profile.option;

import com.sgl.ms.perfil.domain.dto.profile.control.CControl;
import com.sgl.ms.perfil.domain.dto.profile.control.CControlProfile;
import com.sgl.ms.perfil.domain.dto.profile.option.CProfileMenu;
import com.sgl.ms.perfil.domain.dto.profile.option.request.CProfileMenuRequest;
import com.sgl.ms.perfil.domain.dto.response.MenuResponse;
import com.sgl.ms.perfil.domain.dto.response.OptionResponse;
import com.sgl.ms.perfil.domain.repository.profile.control.CControlProfileRepository;
import com.sgl.ms.perfil.domain.repository.profile.control.ControlRepository;
import com.sgl.ms.perfil.domain.repository.profile.option.CProfileMenuRepository;
import com.sgl.ms.perfil.domain.service.profile.control.ControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class CProfileMenuService {
    @Autowired
    private CProfileMenuRepository cProfileMenuRepository;
    @Autowired
    private ControlRepository controlRepository;
    @Autowired
    private CControlProfileRepository cControlProfileRepository;
    @Autowired
    private ControlService controlService;

    public List<MenuResponse> getByProfileId(int profileId) {
        List<MenuResponse> menuResponses = new ArrayList<>();
        List<MenuResponse> resultResponses = new ArrayList<>();
        List<CProfileMenu> cProfileMenus = cProfileMenuRepository.getByProfileId(profileId);

        for (CProfileMenu cp : cProfileMenus) {
            if (cp.getCustomMenu().getLevel() == 1) {
                MenuResponse menuResponse = new MenuResponse();
                menuResponse.setId(cp.getCustomMenu().getId());
                menuResponse.setLabel(cp.getCustomMenu().getDescription());
                menuResponse.setIcon(cp.getCustomMenu().getIcon());
                menuResponse.setPath(cp.getCustomMenu().getUri());
                menuResponse.setSelected(cp.getSelected());
                menuResponse.setIndeterminate(cp.getIndeterminate());
                menuResponse.setOrder(cp.getCustomMenu().getOrder());
                menuResponses.add(menuResponse);
            }
        }

        for (MenuResponse mr : menuResponses) {
            List<OptionResponse> optionResponses = new ArrayList<>();
            for (CProfileMenu ca : cProfileMenus) {
                if (ca.getCustomMenu().getLevel() == 2 && ca.getCustomMenu().getFatherId() == mr.getId()) {
                    OptionResponse optionResponse = new OptionResponse();
                    optionResponse.setId(ca.getCustomMenu().getId());
                    optionResponse.setLabel(ca.getCustomMenu().getDescription());
                    optionResponse.setIcon(ca.getCustomMenu().getIcon());
                    optionResponse.setPath(ca.getCustomMenu().getUri());
                    optionResponse.setSelected(ca.getSelected()); // cambio a peticion de William
                    optionResponse.setIndeterminate(ca.getIndeterminate());
                    optionResponse.setOrder(ca.getCustomMenu().getOrder());
                    optionResponse.setChildren(new ArrayList<>());
                    optionResponses.add(optionResponse);
                }
            }
            mr.setChildren(optionResponses);
        }
        return menuResponses;
    }

    public CProfileMenu findByPerfilIdAndOpcionId(int idPerfil, int idOpcion){
        return cProfileMenuRepository.findByPerfilIdAndOpcionId(idPerfil, idOpcion);
    }

    public List<MenuResponse> save(int idProfile, List<MenuResponse> menuResponses) {
        for (MenuResponse mr : menuResponses) {
            CProfileMenu cFatherProfileMenu = new CProfileMenu();
            for(OptionResponse or : mr.getChildren()) {
                CProfileMenu cProfileMenu = new CProfileMenu();
                cProfileMenu.setUserCreationId(1);
                cProfileMenu.setCreationDate(LocalDateTime.now());
                cProfileMenu.setUserModificationId(1);
                cProfileMenu.setModificationDate(LocalDateTime.now());
                cProfileMenu.setMenuId(or.getId());
                cProfileMenu.setProfileId(idProfile);
                if(or.getSelected() == null){
                    cProfileMenu.setSelected(false);
                }
                else{
                    cProfileMenu.setSelected(or.getSelected());
                }
                if(or.getIndeterminate() == null){
                    cProfileMenu.setIndeterminate(false);
                }
                else{
                    cProfileMenu.setIndeterminate(or.getIndeterminate());
                }
                cProfileMenuRepository.save(cProfileMenu);
            }
            cFatherProfileMenu.setUserCreationId(1);
            cFatherProfileMenu.setCreationDate(LocalDateTime.now());
            cFatherProfileMenu.setUserModificationId(1);
            cFatherProfileMenu.setModificationDate(LocalDateTime.now());
            cFatherProfileMenu.setMenuId(mr.getId());
            cFatherProfileMenu.setProfileId(idProfile);
            if(mr.getSelected() == null){
                cFatherProfileMenu.setSelected(false);
            }
            else{
                cFatherProfileMenu.setSelected(mr.getSelected());
            }
            if(mr.getIndeterminate() == null){
                cFatherProfileMenu.setIndeterminate(false);
            }
            else{
                cFatherProfileMenu.setIndeterminate(mr.getIndeterminate());
            }
            cProfileMenuRepository.save(cFatherProfileMenu);
        }

        // Nuevos cambios para guardar controles
        if(cControlProfileRepository.getByPerfilId(idProfile).get().size() > 0){
            //controlService.deleteByProfileId(idProfile);
            for (MenuResponse ms : menuResponses) {
                for(OptionResponse os : ms.getChildren()) {
                    List<CControl> controls = controlRepository.getByOptionId(os.getId()).get();
                    for(CControl ctrl : controls){
                        if(cControlProfileRepository.getByControlIdAndPerfilId(ctrl.getId(), idProfile).get().size() == 0){
                            boolean flag = os.getSelected() == null ? false : os.getSelected();
                            if(flag){
                                CControlProfile cControlProfile = new CControlProfile();
                                cControlProfile.setUserCreationId(1);
                                cControlProfile.setCreationDate(LocalDateTime.now());
                                cControlProfile.setUserModificationId(1);
                                cControlProfile.setModificationDate(LocalDateTime.now());
                                cControlProfile.setCcontrolId(ctrl.getId());
                                cControlProfile.setProfileId(idProfile);
                                cControlProfile.setFlag(false);
                                cControlProfileRepository.save(cControlProfile);
                            }
                        }
                        else {
                            boolean flag = os.getSelected() == null ? false : os.getSelected();
                            if(!flag){
                                int i = cControlProfileRepository.getByControlIdAndPerfilId(ctrl.getId(), idProfile).get().get(0).getId();
                                cControlProfileRepository.deleteById(i);
                            }
                        }
                    }
                }
            }
        }
        else {
            for (MenuResponse mr : menuResponses) {
                for(OptionResponse or : mr.getChildren()) {
                    boolean flag = or.getSelected() == null ? false : or.getSelected();
                    if(flag){
                        List<CControl> controls = controlRepository.getByOptionId(or.getId()).get();
                        for(CControl ct : controls){
                            CControlProfile cControlProfile = new CControlProfile();
                            cControlProfile.setUserCreationId(1);
                            cControlProfile.setCreationDate(LocalDateTime.now());
                            cControlProfile.setUserModificationId(1);
                            cControlProfile.setModificationDate(LocalDateTime.now());
                            cControlProfile.setCcontrolId(ct.getId());
                            cControlProfile.setProfileId(idProfile);
                            cControlProfile.setFlag(false);
                            cControlProfileRepository.save(cControlProfile);
                        }
                    }
                }
            }
        }
        //return getByProfileId(idProfile);
        return new ArrayList<MenuResponse>();
    }
    @Transactional
    public Integer deleteByProfileId(int profileId) {
        return cProfileMenuRepository.deleteByProfileId(profileId);
    }
}
