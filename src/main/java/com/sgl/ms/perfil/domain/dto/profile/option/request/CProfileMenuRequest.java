package com.sgl.ms.perfil.domain.dto.profile.option.request;

import com.fasterxml.jackson.annotation.JsonValue;
import com.sgl.ms.perfil.domain.dto.response.MenuResponse;

import java.util.List;

public class CProfileMenuRequest {
    //private int idProfile;
    @JsonValue
    List<MenuResponse> menuResponses;

    /*
    public int getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(int idProfile) {
        this.idProfile = idProfile;
    }
    */

    public List<MenuResponse> getMenuResponses() {
        return menuResponses;
    }

    public void setMenuResponses(List<MenuResponse> menuResponses) {
        this.menuResponses = menuResponses;
    }
}
