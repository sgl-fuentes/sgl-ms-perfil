package com.sgl.ms.perfil.domain.service;

import com.fasterxml.jackson.databind.introspect.TypeResolutionContext;
import com.sgl.ms.perfil.domain.dto.*;
import com.sgl.ms.perfil.domain.dto.request.ItemRequest;
import com.sgl.ms.perfil.domain.dto.request.MenuRequest;
import com.sgl.ms.perfil.domain.dto.request.OptionRequest;
import com.sgl.ms.perfil.domain.dto.response.MenuResponse;
import com.sgl.ms.perfil.domain.dto.response.OptionResponse;
import com.sgl.ms.perfil.domain.repository.ItemRepository;
import com.sgl.ms.perfil.domain.repository.MenuRepository;
import com.sgl.ms.perfil.domain.repository.OptionRepository;
import com.sgl.ms.perfil.domain.service.profile.option.CProfileMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class MenuService {
    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private OptionRepository optionRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private ProfileService profileService;
    @Autowired
    private ProfileMenuService profileMenuService;

    /*
    public List<Menu> getAll() {
        return menuRepository.getAll();
    }
    public List<Menu> getAllCollapsable() {
        return menuRepository.getAllCollapsable();
    }
    public List<Menu> findAllMenu(String menuType, String optionType) {
        return menuRepository.findAllMenu(menuType, optionType);
    }

    */
    public List<MenuResponse> findAllTreeMenu(Integer menuLevel) {
        return menuRepository.findAllTreeMenu(menuLevel);
    }
    public List<MenuResponse> findByLevel(Integer menuLevel) {
        List<MenuResponse> menuResponse = menuRepository.findByLevel(menuLevel).stream().sorted(Comparator.comparingInt(MenuResponse::getOrder)).toList();
        //menuResponse.stream().sorted(Comparator.comparingInt(MenuResponse::getOrder)).toList();
        menuResponse.forEach(m -> {m.setChildren(
                m.getChildren().stream().sorted(Comparator.comparingInt(OptionResponse::getOrder)).toList()
        );});

        //return menuRepository.findByLevel(menuLevel);
        return menuResponse;
    }

    public Menu save(MenuRequest menuRequest) {
        Menu menu = new Menu();
        if(menuRequest.getId() != null) {
            menu.setId(menuRequest.getId());
        }
        menu.setDescription(menuRequest.getLabel());
        menu.setIcon(menuRequest.getIcon());
        menu.setUri(menuRequest.getPath());
        menu.setOrder(menuRequest.getOrder());
        menu.setLevel(menuRequest.getLevel());
        menu.setFatherId(menuRequest.getFatherId());
        menu.setUserCreationId(1);
        menu.setUserModificationId(1);
        LocalDateTime now = LocalDateTime.now();
        if(menu.getCreationDate() == null) {
            menu.setCreationDate(now);
        }
        if(menu.getModificationDate() == null) {
            menu.setModificationDate(now);
        }

        Menu preparedMenu = menuRepository.save(menu);

        // Logica que obtiene todos los perfiles
        List<Profile> profiles = profileService.getAll();
        for(Profile p : profiles) {
            ProfileMenu profileMenu = new ProfileMenu();
            profileMenu.setUserCreationId(preparedMenu.getUserCreationId());
            profileMenu.setCreationDate(preparedMenu.getCreationDate());
            profileMenu.setUserModificationId(preparedMenu.getUserModificationId());
            profileMenu.setModificationDate(preparedMenu.getModificationDate());
            profileMenu.setMenuId(preparedMenu.getId());
            profileMenu.setProfileId(p.getId());
            profileMenuService.save(profileMenu);
        }

        return preparedMenu;
    }

    public Option saveOption(OptionRequest optionRequest) {
        Option option = new Option();
        if(optionRequest.getId() != null) {
            option.setId(optionRequest.getId());
        }
        option.setDescription(optionRequest.getLabel());
        option.setIcon(optionRequest.getIcon());
        option.setUri(optionRequest.getPath());
        option.setOrder(optionRequest.getOrder());
        option.setLevel(optionRequest.getLevel());
        option.setFatherId(optionRequest.getFatherId());
        option.setUserCreationId(1);
        option.setUserModificationId(1);
        LocalDateTime now = LocalDateTime.now();
        if(option.getCreationDate() == null) {
            option.setCreationDate(now);
        }
        if(option.getModificationDate() == null) {
            option.setModificationDate(now);
        }

        Option preparedOption = optionRepository.save(option);

        // Logica que obtiene todos los perfiles
        if(optionRequest.getId() == null) {
            List<Profile> profiles = profileService.getAll();
            for(Profile p : profiles) {
                ProfileMenu profileMenu = new ProfileMenu();
                profileMenu.setUserCreationId(preparedOption.getUserCreationId());
                profileMenu.setCreationDate(preparedOption.getCreationDate());
                profileMenu.setUserModificationId(preparedOption.getUserModificationId());
                profileMenu.setModificationDate(preparedOption.getModificationDate());
                profileMenu.setMenuId(preparedOption.getId());
                profileMenu.setProfileId(p.getId());
                profileMenuService.save(profileMenu);
            }
        }

        return preparedOption;
    }

    public Item saveItem(ItemRequest itemRequest) {
        Item item = new Item();
        if(itemRequest.getId() != null) {
            item.setId(itemRequest.getId());
        }
        item.setDescription(itemRequest.getLabel());
        item.setIcon(itemRequest.getIcon());
        item.setUri(itemRequest.getPath());
        item.setOrder(itemRequest.getOrder());
        item.setLevel(itemRequest.getLevel());
        item.setFatherId(itemRequest.getFatherId());
        item.setUserCreationId(1);
        item.setUserModificationId(1);
        LocalDateTime now = LocalDateTime.now();
        if(item.getCreationDate() == null) {
            item.setCreationDate(now);
        }
        if(item.getModificationDate() == null) {
            item.setModificationDate(now);
        }

        Item preparedItem = itemRepository.save(item);

        // Logica que obtiene todos los perfiles
        List<Profile> profiles = profileService.getAll();
        for(Profile p : profiles) {
            ProfileMenu profileMenu = new ProfileMenu();
            profileMenu.setUserCreationId(preparedItem.getUserCreationId());
            profileMenu.setCreationDate(preparedItem.getCreationDate());
            profileMenu.setUserModificationId(preparedItem.getUserModificationId());
            profileMenu.setModificationDate(preparedItem.getModificationDate());
            profileMenu.setMenuId(preparedItem.getId());
            profileMenu.setProfileId(p.getId());
            profileMenuService.save(profileMenu);
        }

        return preparedItem;
    }
    public Optional<Menu> getMenu(int menuId) {
        return menuRepository.getMenu(menuId);
    }
    public boolean delete(int menuId) {
        return getMenu(menuId).map(menu -> {
            menuRepository.delete(menuId);
            return true;
        }).orElse(false);
    }
}
