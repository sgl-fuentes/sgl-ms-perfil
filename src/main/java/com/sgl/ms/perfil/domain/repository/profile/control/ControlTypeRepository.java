package com.sgl.ms.perfil.domain.repository.profile.control;

import com.sgl.ms.perfil.domain.dto.profile.control.ControlType;

import java.util.List;
import java.util.Optional;

public interface ControlTypeRepository {
    List<ControlType> getAll();
    ControlType save(ControlType controlType);
    Optional<ControlType> getControlTypeById(int id);
}
