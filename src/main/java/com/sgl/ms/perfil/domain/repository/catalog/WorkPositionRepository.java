package com.sgl.ms.perfil.domain.repository.catalog;

import com.sgl.ms.perfil.domain.dto.catalog.Group;
import com.sgl.ms.perfil.domain.dto.catalog.IdentityDocumentType;
import com.sgl.ms.perfil.domain.dto.catalog.WorkPosition;

import java.util.List;
import java.util.Optional;

public interface WorkPositionRepository {
    List<WorkPosition> getAll();
    WorkPosition save(WorkPosition workPosition);
    void delete (int workPositionId);
    Optional<WorkPosition> getById(int id);
}
