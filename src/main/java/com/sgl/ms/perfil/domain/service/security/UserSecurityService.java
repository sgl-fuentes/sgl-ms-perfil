package com.sgl.ms.perfil.domain.service.security;

import com.sgl.ms.perfil.persistence.crud.security.UserCrudRepository;
import com.sgl.ms.perfil.persistence.entity.Security.UUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserSecurityService implements UserDetailsService {
    private final UserCrudRepository userCrudRepository;
    @Autowired
    public UserSecurityService(UserCrudRepository userCrudRepository) {
        this.userCrudRepository = userCrudRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UUsuario usuario = this.userCrudRepository.findByCodigo(username)
                .orElseThrow(() -> new UsernameNotFoundException("Usuario" + username + " not found"));
        return User.builder()
                .username(usuario.getCodigo())
                .password(usuario.getClave())
                .build();
    }
}
