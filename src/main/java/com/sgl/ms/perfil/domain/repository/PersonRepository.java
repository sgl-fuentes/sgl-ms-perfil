package com.sgl.ms.perfil.domain.repository;

import com.sgl.ms.perfil.domain.dto.Person;
import com.sgl.ms.perfil.domain.dto.PersonType;

import java.util.List;
import java.util.Optional;

public interface PersonRepository {
    List<Person> getAll();
    Person save(Person person);
    Optional<Person> getById(int personId);
    boolean delete(int personId);
    Optional<List<Person>> findByTipoDocumentoIdentidadIdAndDocumentoIdentidad(int tipoDoc, String nroDoc);
}
