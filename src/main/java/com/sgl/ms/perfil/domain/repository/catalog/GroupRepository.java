package com.sgl.ms.perfil.domain.repository.catalog;

import com.sgl.ms.perfil.domain.dto.catalog.DocumentType;
import com.sgl.ms.perfil.domain.dto.catalog.Group;

import java.util.List;
import java.util.Optional;

public interface GroupRepository {
    List<Group> getAll();
    Group save(Group group);
    void delete (int groupId);
    Optional<Group> getById(int id);
}
