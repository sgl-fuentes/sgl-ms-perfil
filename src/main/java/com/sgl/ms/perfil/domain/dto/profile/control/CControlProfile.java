package com.sgl.ms.perfil.domain.dto.profile.control;

import com.sgl.ms.perfil.domain.dto.Profile;
import lombok.*;

import java.time.LocalDateTime;
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CControlProfile {
    private int id;
    private int userCreationId;
    private LocalDateTime creationDate;
    private int userModificationId;
    private LocalDateTime modificationDate;
    private Integer ccontrolId;
    private Integer profileId;
    private Boolean flag;
    private CControl ccontrol;
    private Profile profile;
}
