package com.sgl.ms.perfil.domain.service;

import com.sgl.ms.perfil.domain.dto.*;
import com.sgl.ms.perfil.domain.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private Environment env;

    public List<Person> getAll() {
        /*
        String url = "http://3.16.216.35:80/sgl/api/cores/get";
        List<Person> personResponse = new ArrayList<>();
        for (Person p : personRepository.getAll()) {
            RestTemplate restTemplate = new RestTemplate();

            String costCenterEntity = "/CostCenter";
            String costCenterUri = url + costCenterEntity + "/" + p.getCostCenterId();
            CostCenter costCenter = restTemplate.getForObject(costCenterUri, CostCenter.class);

            String areaEntity = "/Area";
            String areaUri = url + areaEntity + "/" + p.getRegionId();
            Area area = restTemplate.getForObject(areaUri, Area.class);

            String statusEntity = "/Status";
            String statusUri = url + statusEntity + "/" + p.getStatusId();
            Status status = restTemplate.getForObject(statusUri, Status.class);

            CostCenter responseCostCenter = new CostCenter();
            responseCostCenter.setId(costCenter.getId());
            responseCostCenter.setUserCreationId(costCenter.getUserCreationId());
            responseCostCenter.setCreationDate(costCenter.getCreationDate());
            responseCostCenter.setUserModificationId(costCenter.getUserModificationId());
            responseCostCenter.setModificationDate(costCenter.getModificationDate());
            responseCostCenter.setCode(costCenter.getCode());
            responseCostCenter.setDescription(costCenter.getDescription());
            responseCostCenter.setShortDescription(costCenter.getShortDescription());
            responseCostCenter.setProgramCode(costCenter.getProgramCode());
            responseCostCenter.setFilterCode(costCenter.getFilterCode());
            p.setCostCenter(responseCostCenter);

            Area responseArea = new Area();
            responseArea.setId(area.getId());
            responseArea.setUserCreationId(area.getUserCreationId());
            responseArea.setCreationDate(area.getCreationDate());
            responseArea.setUserModificationId(area.getUserModificationId());
            responseArea.setModificationDate(area.getModificationDate());
            responseArea.setCode(area.getCode());
            responseArea.setDescription(area.getDescription());
            responseArea.setShortDescription(area.getShortDescription());
            responseArea.setProgramCode(area.getProgramCode());
            responseArea.setFilterCode(area.getFilterCode());
            p.setRegion(responseArea);

            Status responseStatus = new Status();
            responseStatus.setId(status.getId());
            responseStatus.setUserCreationId(status.getUserCreationId());
            responseStatus.setCreationDate(status.getCreationDate());
            responseStatus.setUserModificationId(status.getUserModificationId());
            responseStatus.setModificationDate(status.getModificationDate());
            responseStatus.setCode(status.getCode());
            responseStatus.setDescription(status.getDescription());
            responseStatus.setShortDescription(status.getShortDescription());
            responseStatus.setProgramCode(status.getProgramCode());
            responseStatus.setFilterCode(status.getFilterCode());
            p.setStatus(responseStatus);

            personResponse.add(p);
        }
        return personResponse;
        */
        return personRepository.getAll();
    }

    public Person save(Person person) {
        return personRepository.save(person);
    }
    public Optional<Person> getPersonById(int personId) {
        return personRepository.getById(personId);
    }

    public boolean delete(int personId) {
        return getPersonById(personId).map(person -> {
            personRepository.delete(personId);
            return true;
        }).orElse(false);
    }

    public List<Person> findByTipoDocumentoIdentidadIdAndDocumentoIdentidad(int tipoDoc, String nroDoc) {
        return personRepository.findByTipoDocumentoIdentidadIdAndDocumentoIdentidad(tipoDoc, nroDoc).get();
    }
}
