package com.sgl.ms.perfil.domain.service.table;

import com.sgl.ms.perfil.domain.dto.table.SubTable;
import com.sgl.ms.perfil.domain.dto.table.Table;
import com.sgl.ms.perfil.domain.repository.table.SubTableRepository;
import com.sgl.ms.perfil.domain.repository.table.TableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class TableService {
    @Autowired
    private TableRepository tableRepository;
    @Autowired
    private SubTableRepository subTableRepository;
    public Optional<List<Table>> getTableByFatherIsNullAndOptionId(int optionId) {
        List<Table> tables = tableRepository.getByFatherIdIsNullAndOptionId(optionId).get().stream()
                .sorted(Comparator.comparing(Table::getLabel)).toList();
        tables.forEach(m -> {
            m.setChildren(
                    m.getChildren().stream().sorted(Comparator.comparing(SubTable::getLabel)).toList()
            );
        });
        return Optional.of(tables);
    }
}