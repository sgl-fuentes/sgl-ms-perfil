package com.sgl.ms.perfil.domain.repository;

import com.sgl.ms.perfil.domain.dto.Menu;
import com.sgl.ms.perfil.domain.dto.Product;
import com.sgl.ms.perfil.domain.dto.response.MenuResponse;
import java.util.List;
import java.util.Optional;

public interface MenuRepository {
    //List<Menu> getAll();
    //List<Menu> getAllCollapsable();
    //List<Menu> findAllMenu(String menuType, String optionType);
    List<MenuResponse> findAllTreeMenu(Integer menuLevel);
    List<MenuResponse> findByLevel(Integer menuLevel);
    Menu save(Menu menu);
    Optional<Menu> getMenu(int menuId);
    void delete (int menuId);
}
