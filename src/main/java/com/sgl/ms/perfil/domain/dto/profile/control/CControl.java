package com.sgl.ms.perfil.domain.dto.profile.control;

import com.sgl.ms.perfil.domain.dto.Option;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CControl {
    private int id;
    private int userCreationId;
    private LocalDateTime creationDate;
    private int userModificationId;
    private LocalDateTime modificationDate;
    private String code;
    private String description;
    private String shortDescription;
    private String programCode;
    private String filterCode;
    private Integer optionId;
    private String control;
    private String html;
    private Integer controlTypeId;
    private ControlType controlType;
    private Option option;
}
