package com.sgl.ms.perfil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SglMsPerfilApplication {
	public static void main(String[] args) {
		SpringApplication.run(SglMsPerfilApplication.class, args);
	}
}

