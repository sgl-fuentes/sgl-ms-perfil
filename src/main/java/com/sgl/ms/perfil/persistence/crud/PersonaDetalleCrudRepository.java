package com.sgl.ms.perfil.persistence.crud;

import com.sgl.ms.perfil.persistence.entity.PersonaDetalle;
import com.sgl.ms.perfil.persistence.entity.Producto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PersonaDetalleCrudRepository extends CrudRepository<PersonaDetalle, Integer> {
    Optional<List<PersonaDetalle>> findByPersonaId (int personId);
}
