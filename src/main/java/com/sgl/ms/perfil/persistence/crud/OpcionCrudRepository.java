package com.sgl.ms.perfil.persistence.crud;

import com.sgl.ms.perfil.persistence.entity.Opcion;
import org.springframework.data.repository.CrudRepository;

public interface OpcionCrudRepository extends CrudRepository<Opcion, Integer> {

}
