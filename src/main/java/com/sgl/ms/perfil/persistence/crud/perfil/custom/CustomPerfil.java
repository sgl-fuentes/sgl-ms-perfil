package com.sgl.ms.perfil.persistence.crud.perfil.custom;

import java.time.LocalDateTime;

public interface CustomPerfil {
    Integer getId();
    Integer getUsuarioCreacionId();
    LocalDateTime getFechaCreacion();
    Integer getUsuarioActualizacionId();
    LocalDateTime getFechaActualizacion();
    String getCodigo();
    String getDescripcion();
    String getDescripcionCorta();
    String getCodigoPrograma();
    String getCodigoFiltro();
    Integer getOId();
    Integer getOUsuarioCreacionId();
    LocalDateTime getOFechaCreacion();
    Integer getUsuarioOActualizacionId();
    LocalDateTime getOFechaActualizacion();
    Integer getOOpcionId();
    Integer getOPerfilId();
    Boolean getOSelected();
    Boolean getOIndeterminate();
}
