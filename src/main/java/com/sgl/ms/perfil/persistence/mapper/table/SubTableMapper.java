package com.sgl.ms.perfil.persistence.mapper.table;

import com.sgl.ms.perfil.domain.dto.table.SubTable;
import com.sgl.ms.perfil.persistence.entity.tabla.SubTabla;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SubTableMapper {
    @Mappings({
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "descripcion", target = "label"),
            @Mapping(source = "orden", target = "order"),
    })
    SubTable toSubTable(SubTabla subTabla);
    List<SubTable> toSubTables(List<SubTabla> subTablas);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "usuarioCreacionId", ignore = true),
            @Mapping(target = "fechaCreacion", ignore = true),
            @Mapping(target = "usuarioActualizacionId", ignore = true),
            @Mapping(target = "fechaActualizacion", ignore = true),
            @Mapping(target = "descripcionCorta", ignore = true),
            @Mapping(target = "codigoPrograma", ignore = true),
            @Mapping(target = "codigoFiltro", ignore = true),
            @Mapping(target = "opcionId", ignore = true),
            @Mapping(target = "icono", ignore = true),
            @Mapping(target = "url", ignore = true),
            @Mapping(target = "tablaPadreId", ignore = true),
            @Mapping(target = "tipoTablaId", ignore = true),
            @Mapping(target = "nivel", ignore = true),
            @Mapping(target = "tabla", ignore = true)
    })
    SubTabla toSubTabla(SubTable subTable);
}
