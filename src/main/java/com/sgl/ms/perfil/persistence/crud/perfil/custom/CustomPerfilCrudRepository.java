package com.sgl.ms.perfil.persistence.crud.perfil.custom;

import com.sgl.ms.perfil.persistence.crud.perfil.custom.CustomPerfil;
import com.sgl.ms.perfil.persistence.entity.UsuarioPerfil;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomPerfilCrudRepository extends CrudRepository<UsuarioPerfil, Integer> {
    @Query(value = """
            select p.id, p.usuario_creacion_id, p.fecha_creacion, p.usuario_actualizacion_id, p.fecha_actualizacion,
            p.codigo, p.descripcion, p.descripcion_corta, p.codigo_programa, p.codigo_filtro,
            op.id oid, op.usuario_creacion_id ousuario_creacion_id, op.fecha_creacion ofecha_creacion, op.usuario_actualizacion_id ousuario_actualizacion_id, op.fecha_actualizacion ofecha_actualizacion,
            op.opcion_id oopcion_id, op.perfil_id operfil_id, op.selected oselected, op.indeterminate oindeterminate
            from dev.perfil p inner join dev.opcion_perfil op
            on p.id = op.perfil_id
            inner join dev.opcion o
            on op.opcion_id = o.id
            where o.nivel = 1
            """, nativeQuery = true)
    List<CustomPerfil> getCustomPerfil();
}
