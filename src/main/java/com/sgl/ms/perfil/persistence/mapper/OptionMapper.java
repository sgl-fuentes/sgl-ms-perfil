package com.sgl.ms.perfil.persistence.mapper;

import com.sgl.ms.perfil.domain.dto.Option;
import com.sgl.ms.perfil.persistence.entity.Opcion;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {ItemMapper.class})
public interface OptionMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "descripcion", target = "description"),
            @Mapping(source = "descripcionCorta", target = "shortDescription"),
            @Mapping(source = "codigoPrograma", target = "programCode"),
            @Mapping(source = "codigoFiltro", target = "filterCode"),
            @Mapping(source = "icono", target = "icon"),
            @Mapping(source = "url", target = "uri"),
            @Mapping(source = "opcionPadreId", target = "fatherId"),
            @Mapping(source = "orden", target = "order"),
            @Mapping(source = "nivel", target = "level"),
            @Mapping(source = "subOpciones", target = "items")
    })
    Option toOption(Opcion opcion);
    List<Option> toOptions(List<Opcion> opcion);
    @InheritInverseConfiguration
    @Mappings({
        @Mapping(target = "barra", ignore = true),
        @Mapping(target = "controles", ignore = true)
    })
    Opcion toOpcion(Option option);
}
