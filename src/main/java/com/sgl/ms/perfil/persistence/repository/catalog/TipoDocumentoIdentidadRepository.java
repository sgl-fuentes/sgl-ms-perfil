package com.sgl.ms.perfil.persistence.repository.catalog;

import com.sgl.ms.perfil.domain.dto.catalog.IdentityDocumentType;
import com.sgl.ms.perfil.domain.repository.catalog.IdentityDocumentTypeRepository;
import com.sgl.ms.perfil.persistence.crud.catalog.TipoDocumentoIdentidadCrudRepository;
import com.sgl.ms.perfil.persistence.entity.catalog.maestro.Agrupacion;
import com.sgl.ms.perfil.persistence.entity.catalog.maestro.TipoDocumentoIdentidad;
import com.sgl.ms.perfil.persistence.mapper.catalog.IdentityDocumentTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TipoDocumentoIdentidadRepository implements IdentityDocumentTypeRepository {
    @Autowired
    private TipoDocumentoIdentidadCrudRepository tipoDocumentoIdentidadCrudRepository;
    @Autowired
    private IdentityDocumentTypeMapper identityDocumentTypeMapper;

    @Override
    public List<IdentityDocumentType> getAll() {
        List<TipoDocumentoIdentidad> tipoDocumentoIdentidades = (List<TipoDocumentoIdentidad>) tipoDocumentoIdentidadCrudRepository.findAll();
        return identityDocumentTypeMapper.toIdentityDocumentTypes(tipoDocumentoIdentidades);
    }

    @Override
    public IdentityDocumentType save(IdentityDocumentType identityDocumentType) {
        TipoDocumentoIdentidad tipoDocumentoIdentidad = identityDocumentTypeMapper.toTipoDocumentoIdentidad(identityDocumentType);
        return identityDocumentTypeMapper.toIdentityDocumentType(tipoDocumentoIdentidadCrudRepository.save(tipoDocumentoIdentidad));
    }

    @Override
    public void delete(int identityDocumentTypeId) {
        tipoDocumentoIdentidadCrudRepository.deleteById(identityDocumentTypeId);
    }

    @Override
    public Optional<IdentityDocumentType> getById(int id) {
        return tipoDocumentoIdentidadCrudRepository.findById(id).map(i -> identityDocumentTypeMapper.toIdentityDocumentType(i));
    }
}
