package com.sgl.ms.perfil.persistence.crud;

import com.sgl.ms.perfil.persistence.entity.SubOpcion;
import org.springframework.data.repository.CrudRepository;

public interface SubOpcionCrudRepository extends CrudRepository<SubOpcion, Integer> {

}
