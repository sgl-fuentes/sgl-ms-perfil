package com.sgl.ms.perfil.persistence.repository.tabla;

import com.sgl.ms.perfil.domain.dto.table.SubTable;
import com.sgl.ms.perfil.domain.repository.table.SubTableRepository;
import com.sgl.ms.perfil.persistence.crud.tabla.SubTablaCrudRepository;
import com.sgl.ms.perfil.persistence.entity.tabla.SubTabla;
import com.sgl.ms.perfil.persistence.mapper.table.SubTableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class SubTablaRepository implements SubTableRepository {
    @Autowired
    private SubTablaCrudRepository subTablaCrudRepository;
    @Autowired
    private SubTableMapper subTableMapper;
    @Override
    public Optional<List<SubTable>> getByOptionId(int optionId) {
        Optional<List<SubTabla>> subTablas = subTablaCrudRepository.findByOpcionId(optionId);
        return Optional.of(subTableMapper.toSubTables(subTablas.get()));
    }
}
