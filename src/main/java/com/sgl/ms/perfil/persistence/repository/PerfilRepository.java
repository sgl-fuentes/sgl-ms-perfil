package com.sgl.ms.perfil.persistence.repository;

import com.sgl.ms.perfil.domain.dto.Profile;
import com.sgl.ms.perfil.domain.dto.response.UserByProfileResponse;
import com.sgl.ms.perfil.domain.repository.ProfileRepository;
import com.sgl.ms.perfil.persistence.crud.PerfilCrudRepository;
import com.sgl.ms.perfil.persistence.entity.Perfil;
import com.sgl.ms.perfil.persistence.mapper.ProfileMapper;
import com.sgl.ms.perfil.persistence.mapper.perfil.menu.UserByProfileResponseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PerfilRepository implements ProfileRepository {
    @Autowired
    private PerfilCrudRepository perfilCrudRepository;
    @Autowired
    private ProfileMapper mapper;
    @Autowired
    private UserByProfileResponseMapper userByProfileMapper;

    @Override
    public List<Profile> getAll() {
        List<Perfil> perfiles = (List<Perfil>) perfilCrudRepository.findAll();
        return mapper.toProfiles(perfiles);
    }

    @Override
    public List<Profile> getByDescription(String description) {
        List<Perfil> perfiles = perfilCrudRepository.findByDescripcion(description);
        return mapper.toProfiles(perfiles);
    }

    @Override
    public Optional<Profile> getById(int idProfile) {
        return perfilCrudRepository.findById(idProfile).map(perfil -> mapper.toProfile(perfil));
    }

    @Override
    public Optional<UserByProfileResponse> getUserByProfileId(int idProfile) {
        return perfilCrudRepository.findById(idProfile).map(perfil -> userByProfileMapper.toUserByProfile(perfil));
    }

    @Override
    public Profile save(Profile profile) {
        Perfil perfil = mapper.toPerfil(profile);
        return mapper.toProfile(perfilCrudRepository.save(perfil));
    }
}
