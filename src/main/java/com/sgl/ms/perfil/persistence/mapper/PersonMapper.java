package com.sgl.ms.perfil.persistence.mapper;

import com.sgl.ms.perfil.domain.dto.Person;
import com.sgl.ms.perfil.persistence.entity.PPersona;
import com.sgl.ms.perfil.persistence.mapper.catalog.*;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {IdentityDocumentTypeMapper.class, WorkPositionMapper.class, GroupMapper.class, CostCenterMapper.class,
        AreaMapper.class, StatusMapper.class, PersonTypeMapper.class})
public interface PersonMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "tipoDocumentoIdentidadId", target = "identityDocumentTypeId"),
            @Mapping(source = "documentoIdentidad", target = "identityDocument"),
            @Mapping(source = "apellidoPaterno", target = "lastName"),
            @Mapping(source = "apellidoMaterno", target = "secondLastName"),
            @Mapping(source = "primeroNombre", target = "firstName"),
            @Mapping(source = "segundoNombre", target = "secondName"),
            @Mapping(source = "email", target = "email"),
            @Mapping(source = "telefonoCelular", target = "phone"),
            @Mapping(source = "cargoId", target = "jobId"),
            @Mapping(source = "agrupacionId", target = "groupId"),
            @Mapping(source = "fechaIngreso", target = "hireDate"),
            @Mapping(source = "regionId", target = "regionId"),
            @Mapping(source = "estadoId", target = "statusId"),
            @Mapping(source = "centroCostoId", target = "costCenterId"),
            @Mapping(source = "sueldo", target = "salary"),
            @Mapping(source = "tipoPersonaId", target = "personTypeId"),
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "tipoDocumentoIdentidad", target = "identityDocumentType"),
            @Mapping(source = "cargo", target = "job"),
            @Mapping(source = "agrupacion", target = "group"),
            @Mapping(source = "centroCosto", target = "costCenter"),
            @Mapping(source = "region", target = "region"),
            @Mapping(source = "estado", target = "status"),
            @Mapping(source = "tipoPersona", target = "personType")
    })
    Person toPerson(PPersona persona);
    List<Person> toPersons(List<PPersona> personas);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "personaDetalles", ignore = true),
    })
    PPersona toPersona(Person person);
}
