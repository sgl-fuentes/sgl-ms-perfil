package com.sgl.ms.perfil.persistence.crud.perfil.control;

import com.sgl.ms.perfil.persistence.entity.perfil.control.Control;
import com.sgl.ms.perfil.persistence.entity.perfil.control.ControlPerfil;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ControlCrudRepository extends CrudRepository<Control, Integer> {
    List<Control> findByOpcionId(int idOpcion);
}
