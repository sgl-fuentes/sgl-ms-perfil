package com.sgl.ms.perfil.persistence.repository;

import com.sgl.ms.perfil.domain.dto.ProfileMenu;
import com.sgl.ms.perfil.domain.repository.ProfileMenuRepository;
import com.sgl.ms.perfil.persistence.crud.PerfilBarraCrudRepository;
import com.sgl.ms.perfil.persistence.entity.PerfilBarra;
import com.sgl.ms.perfil.persistence.mapper.ProfileMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PerfilBarraRepository implements ProfileMenuRepository {

    @Autowired
    private ProfileMenuMapper profileMenuMapper;
    @Autowired
    private PerfilBarraCrudRepository perfilBarraCrudRepository;

    @Override
    public ProfileMenu save(ProfileMenu profileMenu) {
        PerfilBarra perfilBarra = profileMenuMapper.toPerfilBarra(profileMenu);
        return profileMenuMapper.toProfileMenu(perfilBarraCrudRepository.save(perfilBarra));
    }
}
