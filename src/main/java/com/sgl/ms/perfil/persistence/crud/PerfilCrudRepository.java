package com.sgl.ms.perfil.persistence.crud;

import com.sgl.ms.perfil.persistence.entity.Perfil;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PerfilCrudRepository extends CrudRepository<Perfil, Integer> {
    List<Perfil> findByDescripcion (String descripcion);

}
