package com.sgl.ms.perfil.persistence.mapper;

import com.sgl.ms.perfil.domain.dto.PersonType;
import com.sgl.ms.perfil.persistence.entity.TipoPersona;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PersonTypeMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "descripcion", target = "description"),
            @Mapping(source = "descripcionCorta", target = "shortDescription"),
            @Mapping(source = "codigoPrograma", target = "programCode"),
            @Mapping(source = "codigoFiltro", target = "filterCode"),
    })
    PersonType toPersonType(TipoPersona tipoPersona);
    List<PersonType> toPersonTypes(List<TipoPersona> tipoPersonas);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "personas", ignore = true),
    })
    TipoPersona toTipoPersona(PersonType personType);
}
