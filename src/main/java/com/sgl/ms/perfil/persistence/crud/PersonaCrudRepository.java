package com.sgl.ms.perfil.persistence.crud;

import com.sgl.ms.perfil.persistence.entity.PPersona;
import com.sgl.ms.perfil.persistence.entity.Producto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PersonaCrudRepository extends CrudRepository<PPersona, Integer> {
    Optional<PPersona> findById(int personId);
    Optional<List<PPersona>> findByTipoDocumentoIdentidadIdAndDocumentoIdentidad(int tipoDoc, String nroDoc);
}
