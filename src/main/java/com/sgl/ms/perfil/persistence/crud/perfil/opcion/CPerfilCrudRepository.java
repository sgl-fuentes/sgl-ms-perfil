package com.sgl.ms.perfil.persistence.crud.perfil.opcion;

import com.sgl.ms.perfil.persistence.entity.perfil.opcion.CPerfil;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CPerfilCrudRepository extends CrudRepository<CPerfil, Integer> {
    @Query(value = "select p.id, p.usuario_creacion_id, p.fecha_creacion, p.usuario_actualizacion_id, p.fecha_actualizacion, " +
            "p.codigo, p.descripcion, p.descripcion_corta, p.codigo_programa, p.codigo_filtro " +
            "from dev.opcion_perfil op " +
            "inner join dev.perfil p " +
            "on p.id = op.perfil_id " +
            "where p.id = 1 " , nativeQuery=true)
    List<CPerfil> findByFirstLevel();
}
