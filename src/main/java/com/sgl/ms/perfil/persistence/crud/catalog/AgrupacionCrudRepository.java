package com.sgl.ms.perfil.persistence.crud.catalog;

import com.sgl.ms.perfil.persistence.entity.catalog.maestro.Agrupacion;
import org.springframework.data.repository.CrudRepository;

public interface AgrupacionCrudRepository extends CrudRepository<Agrupacion, Integer> {
}
