package com.sgl.ms.perfil.persistence.mapper.table;

import com.sgl.ms.perfil.domain.dto.Menu;
import com.sgl.ms.perfil.domain.dto.table.Table;
import com.sgl.ms.perfil.persistence.entity.Barra;
import com.sgl.ms.perfil.persistence.entity.tabla.Tabla;
import com.sgl.ms.perfil.persistence.mapper.OptionMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {SubTableMapper.class})
public interface TableMapper {
    @Mappings({
            @Mapping(source = "codigo", target = "label"),
            @Mapping(source = "subTablas", target = "children"),
            @Mapping(source = "orden", target = "order"),
    })
    Table toTable(Tabla tabla);
    List<Table> toTables(List<Tabla> tablas);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "usuarioCreacionId", ignore = true),
            @Mapping(target = "fechaCreacion", ignore = true),
            @Mapping(target = "usuarioActualizacionId", ignore = true),
            @Mapping(target = "fechaActualizacion", ignore = true),
            @Mapping(target = "descripcion", ignore = true),
            @Mapping(target = "descripcionCorta", ignore = true),
            @Mapping(target = "codigoPrograma", ignore = true),
            @Mapping(target = "codigoFiltro", ignore = true),
            @Mapping(target = "opcionId", ignore = true),
            @Mapping(target = "icono", ignore = true),
            @Mapping(target = "url", ignore = true),
            @Mapping(target = "tablaPadreId", ignore = true),
            //@Mapping(target = "orden", ignore = true),
            @Mapping(target = "tipoTablaId", ignore = true),
            @Mapping(target = "nivel", ignore = true),
    })
    Tabla toTabla(Table table);
}
