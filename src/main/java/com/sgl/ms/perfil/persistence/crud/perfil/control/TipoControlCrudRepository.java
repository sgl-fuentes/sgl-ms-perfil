package com.sgl.ms.perfil.persistence.crud.perfil.control;

import com.sgl.ms.perfil.persistence.entity.perfil.control.TipoControl;
import org.springframework.data.repository.CrudRepository;

public interface TipoControlCrudRepository extends CrudRepository<TipoControl, Integer> {

}
