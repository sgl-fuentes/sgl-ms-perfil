package com.sgl.ms.perfil.persistence.crud.catalog;

import com.sgl.ms.perfil.persistence.entity.catalog.maestro.TipoDocumentoIdentidad;
import org.springframework.data.repository.CrudRepository;

public interface TipoDocumentoIdentidadCrudRepository extends CrudRepository<TipoDocumentoIdentidad, Integer> {
}
