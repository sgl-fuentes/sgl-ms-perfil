package com.sgl.ms.perfil.persistence.mapper.perfil.menu;

import com.sgl.ms.perfil.domain.dto.response.UserByProfileResponse;
import com.sgl.ms.perfil.persistence.entity.Perfil;
import com.sgl.ms.perfil.persistence.mapper.UserProfileMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserProfileMapper.class})
public interface UserByProfileResponseMapper {
    @Mappings({
            @Mapping(source = "usuarios", target = "users")
    })
    UserByProfileResponse toUserByProfile(Perfil perfil);
    List<UserByProfileResponse> toUserByProfiles(List<Perfil> perfiles);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "codigoFiltro", ignore = true),
            @Mapping(target = "codigoPrograma", ignore = true),
            @Mapping(target = "descripcionCorta", ignore = true),
            @Mapping(target = "descripcion", ignore = true),
            @Mapping(target = "codigo", ignore = true),
            @Mapping(target = "fechaActualizacion", ignore = true),
            @Mapping(target = "usuarioActualizacionId", ignore = true),
            @Mapping(target = "fechaCreacion", ignore = true),
            @Mapping(target = "usuarioCreacionId", ignore = true),
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "usuarios", ignore = true),
            @Mapping(target = "controlPerfiles", ignore = true)
    })
    Perfil toPerfil(UserByProfileResponse userByProfile);
}
