package com.sgl.ms.perfil.persistence.mapper;


import com.sgl.ms.perfil.domain.dto.Profile;
import com.sgl.ms.perfil.persistence.entity.Perfil;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring" , uses = {UserProfileMapper.class})
public interface ProfileMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "descripcion", target = "description"),
            @Mapping(source = "descripcionCorta", target = "shortDescription"),
            @Mapping(source = "codigoPrograma", target = "programCode"),
            @Mapping(source = "codigoFiltro", target = "filterCode")
            //@Mapping(source = "usuarios", target = "users")
    })
    Profile toProfile(Perfil perfil);
    List<Profile> toProfiles(List<Perfil> perfiles);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "usuarios", ignore = true),
            @Mapping(target = "controlPerfiles", ignore = true),
    })
    Perfil toPerfil(Profile profile);
}
