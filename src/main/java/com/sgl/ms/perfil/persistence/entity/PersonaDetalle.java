package com.sgl.ms.perfil.persistence.entity;

import com.sgl.ms.perfil.persistence.entity.catalog.maestro.Estado;
import com.sgl.ms.perfil.persistence.entity.catalog.maestro.TipoDocumento;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "persona_detalle")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PersonaDetalle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "usuario_creacion_id")
    private Integer usuarioCreacionId;
    @Column(name = "fecha_creacion")
    private LocalDateTime fechaCreacion;
    @Column(name = "usuario_actualizacion_id")
    private Integer usuarioActualizacionId;
    @Column(name = "fecha_actualizacion")
    private LocalDateTime fechaActualizacion;
    @Column(name = "persona_id")
    private Integer personaId;
    @Column(name = "tipo_documento_id")
    private Integer tipoDocumentoId;
    @Column(name = "imagen", columnDefinition="bytea")
    private byte[] imagen;
    //INI Nuevos Campos
    @Column(name = "fecha_vencimiento")
    private LocalDate fechaVencimiento;
    @Column(name = "tipo_detalle_id")
    private Integer tipoDetalleId;
    @Column(name = "empresa_id")
    private Integer empresaId;
    @Column(name = "cuenta")
    private String cuenta;
    @Column(name = "cuenta_cci")
    private String cuentaCci;
    @Column(name = "estado_id")
    private Integer estadoId;
    //FIN Nuevos Campos
    @ManyToOne
    @JoinColumn(name = "persona_id", referencedColumnName = "id", insertable = false, updatable = false)
    private PPersona persona;
    @ManyToOne
    @JoinColumn(name = "tipo_documento_id", referencedColumnName = "id", insertable = false, updatable = false)
    private TipoDocumento tipoDocumento;
    //INI Nuevos Campos
    @ManyToOne
    @JoinColumn(name = "estado_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Estado estado;
    //FIN Nuevos Campos
}
