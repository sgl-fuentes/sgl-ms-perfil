package com.sgl.ms.perfil.persistence.crud.tabla;

import com.sgl.ms.perfil.persistence.entity.tabla.SubTabla;
import com.sgl.ms.perfil.persistence.entity.tabla.Tabla;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SubTablaCrudRepository extends CrudRepository<SubTabla, Integer> {
    Optional<List<SubTabla>> findByOpcionId(int opcionId);
}
