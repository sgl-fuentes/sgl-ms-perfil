package com.sgl.ms.perfil.persistence.repository.catalog;

import com.sgl.ms.perfil.domain.dto.catalog.Group;
import com.sgl.ms.perfil.domain.repository.catalog.GroupRepository;
import com.sgl.ms.perfil.persistence.crud.catalog.AgrupacionCrudRepository;
import com.sgl.ms.perfil.persistence.entity.catalog.maestro.Agrupacion;
import com.sgl.ms.perfil.persistence.entity.catalog.maestro.TipoDocumento;
import com.sgl.ms.perfil.persistence.mapper.catalog.GroupMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AgrupacionRepository implements GroupRepository {
    @Autowired
    private AgrupacionCrudRepository agrupacionCrudRepository;
    @Autowired
    private GroupMapper groupMapper;

    @Override
    public List<Group> getAll() {
        List<Agrupacion> agrupaciones = (List<Agrupacion>) agrupacionCrudRepository.findAll();
        return groupMapper.toGroups(agrupaciones);
    }

    @Override
    public Group save(Group group) {
        Agrupacion agrupacion = groupMapper.toAgrupacion(group);
        return groupMapper.toGroup(agrupacionCrudRepository.save(agrupacion));
    }

    @Override
    public void delete(int groupId) {
        agrupacionCrudRepository.deleteById(groupId);
    }

    @Override
    public Optional<Group> getById(int id) {
        return agrupacionCrudRepository.findById(id).map(group -> groupMapper.toGroup(group));
    }
}
