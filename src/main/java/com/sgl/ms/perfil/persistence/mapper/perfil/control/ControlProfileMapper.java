package com.sgl.ms.perfil.persistence.mapper.perfil.control;

import com.sgl.ms.perfil.domain.dto.profile.control.CControlProfile;
import com.sgl.ms.perfil.persistence.entity.perfil.control.ControlPerfil;
import com.sgl.ms.perfil.persistence.mapper.ProfileMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring",uses = {ControlMapper.class, ProfileMapper.class})
public interface ControlProfileMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "controlId", target = "ccontrolId"),
            @Mapping(source = "perfilId", target = "profileId"),
            @Mapping(source = "flag", target = "flag"),
            @Mapping(source = "control", target = "ccontrol"),
            @Mapping(source = "perfil", target = "profile")
    })
    CControlProfile toCControlProfile(ControlPerfil controlPerfil);
    List<CControlProfile> toCControlProfiles(List<ControlPerfil> controlPerfiles);
    @InheritInverseConfiguration
    ControlPerfil toControlPerfil(CControlProfile cControlProfile);
}
