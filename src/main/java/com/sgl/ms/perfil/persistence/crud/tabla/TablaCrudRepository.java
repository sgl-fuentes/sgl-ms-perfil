package com.sgl.ms.perfil.persistence.crud.tabla;

import com.sgl.ms.perfil.persistence.entity.Compra;
import com.sgl.ms.perfil.persistence.entity.tabla.Tabla;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TablaCrudRepository extends CrudRepository<Tabla, Integer> {
    Optional<List<Tabla>> findByTablaPadreIdIsNullAndOpcionId(int opcionId);
}
