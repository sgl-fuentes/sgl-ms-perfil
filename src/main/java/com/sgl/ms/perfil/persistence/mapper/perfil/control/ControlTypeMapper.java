package com.sgl.ms.perfil.persistence.mapper.perfil.control;

import com.sgl.ms.perfil.domain.dto.profile.control.ControlType;
import com.sgl.ms.perfil.persistence.entity.perfil.control.TipoControl;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ControlTypeMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "descripcion", target = "description"),
            @Mapping(source = "descripcionCorta", target = "shortDescription"),
            @Mapping(source = "codigoPrograma", target = "programCode"),
            @Mapping(source = "codigoFiltro", target = "filterCode")
    })
    ControlType toControlType(TipoControl tipoControl);
    List<ControlType> toControlTypes(List<TipoControl> tipoControles);
    @InheritInverseConfiguration
    @Mapping(target = "controles", ignore = true)
    TipoControl toTipoControl(ControlType controlType);
}
