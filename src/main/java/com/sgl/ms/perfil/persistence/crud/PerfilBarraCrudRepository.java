package com.sgl.ms.perfil.persistence.crud;

import com.sgl.ms.perfil.persistence.entity.PerfilBarra;
import org.springframework.data.repository.CrudRepository;

public interface PerfilBarraCrudRepository extends CrudRepository<PerfilBarra, Integer> {

}
