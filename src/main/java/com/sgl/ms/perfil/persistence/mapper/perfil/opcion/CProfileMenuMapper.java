package com.sgl.ms.perfil.persistence.mapper.perfil.opcion;

import com.sgl.ms.perfil.domain.dto.profile.option.CProfileMenu;
import com.sgl.ms.perfil.persistence.entity.perfil.opcion.CPerfilBarra;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CMenuMapper.class, CProfileMapper.class})
public interface CProfileMenuMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "barraId", target = "menuId"),
            @Mapping(source = "perfilId", target = "profileId"),
            @Mapping(source = "selected", target = "selected"),
            @Mapping(source = "indeterminate", target = "indeterminate"),
            @Mapping(source = "cPerfil", target = "customProfile"),
            @Mapping(source = "cBarra", target = "customMenu")
    })
    CProfileMenu toCProfileMenu(CPerfilBarra cPerfilBarra);
    List<CProfileMenu> toCProfileMenus(List<CPerfilBarra> cPerfilBarras);
    @InheritInverseConfiguration
    CPerfilBarra toCPerfilBarra(CProfileMenu cProfileMenu);
}
