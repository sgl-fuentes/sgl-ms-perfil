package com.sgl.ms.perfil.persistence;

import com.sgl.ms.perfil.domain.dto.Product;
import com.sgl.ms.perfil.domain.dto.Purchase;
import com.sgl.ms.perfil.domain.repository.ProductRepository;
import com.sgl.ms.perfil.domain.repository.PurchaseRepository;
import com.sgl.ms.perfil.persistence.crud.CompraCrudRepository;
import com.sgl.ms.perfil.persistence.crud.ProductoCrudRepository;
import com.sgl.ms.perfil.persistence.entity.Compra;
import com.sgl.ms.perfil.persistence.entity.Producto;
import com.sgl.ms.perfil.persistence.mapper.ProductMapper;
import com.sgl.ms.perfil.persistence.mapper.PurchaseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductoRepository implements ProductRepository {
    @Autowired
    private ProductoCrudRepository productoCrudRepository;
    @Autowired
    private ProductMapper mapper;

    @Override
    public List<Product> getAll() {
        List<Producto> productos = (List<Producto>) productoCrudRepository.findAll();
        return mapper.toProducts(productos);
    }
    @Override
    public Optional<List<Product>> getByCategory(int categoryId) {
        List<Producto> productos = productoCrudRepository.findByIdCategoriaOrderByNombreAsc(categoryId);
        return Optional.of(mapper.toProducts(productos));
    }
    @Override
    public Optional<List<Product>> getScarseProducts(int quantity) {
        Optional<List<Producto>> productos = productoCrudRepository.findByCantidadStockLessThanAndEstado(quantity, true);
        return productos.map(prods -> mapper.toProducts(prods));
    }
    @Override
    public Optional<Product> getProduct(int productId) {
        return productoCrudRepository.findById(productId).map(producto -> mapper.toProduct(producto));
    }
    @Override
    public Product save(Product product) {
        Producto producto = mapper.toProducto(product);
        return mapper.toProduct(productoCrudRepository.save(producto));
    }
    @Override
    public void delete(int productId) {
        productoCrudRepository.deleteById(productId);
    }
    /*
    public List<Producto> getAll() {
        return (List<Producto>) productoCrudRepository.findAll();
    }

    public List<Producto> getByCategoria(int idCatetoria) {
        return productoCrudRepository.findByIdCategoriaOrderByNombreAsc(idCatetoria);
    }

    public Optional<List<Producto>> getEscasos(int cantidad) {
        return productoCrudRepository.findByCantidadStockLessThanAndEstado(cantidad, true);
    }

    public Optional<Producto> getProducto(int idProducto) {
        return productoCrudRepository.findById(idProducto);
    }

    public Producto save(Producto producto) {
        return productoCrudRepository.save(producto);
    }

    public void delete(int idProducto) {
        productoCrudRepository.deleteById(idProducto);
    }
    */

    @Repository
    public static class CompraRepository implements PurchaseRepository {
        @Autowired
        private CompraCrudRepository compraCrudRepository;
        @Autowired
        private PurchaseMapper purchaseMapper;

        @Override
        public List<Purchase> getAll() {
            return purchaseMapper.toPurchases((List<Compra>) compraCrudRepository.findAll());
        }
        @Override
        public Optional<List<Purchase>> getByClient(String clientId) {
            return compraCrudRepository.findByIdCliente(clientId)
                    .map(compras -> purchaseMapper.toPurchases(compras));
        }
        @Override
        public Purchase save(Purchase purchase) {
            Compra compra = purchaseMapper.toCompra(purchase);
            compra.getProductos().forEach(producto -> producto.setCompra(compra));
            return purchaseMapper.toPurchase(compraCrudRepository.save(compra));
        }
    }
}
