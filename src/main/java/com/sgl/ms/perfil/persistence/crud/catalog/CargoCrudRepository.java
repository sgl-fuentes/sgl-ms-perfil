package com.sgl.ms.perfil.persistence.crud.catalog;

import com.sgl.ms.perfil.persistence.entity.catalog.maestro.Cargo;
import org.springframework.data.repository.CrudRepository;

public interface CargoCrudRepository extends CrudRepository<Cargo, Integer> {
}
