package com.sgl.ms.perfil.persistence.crud;

import com.sgl.ms.perfil.persistence.entity.TipoPersona;
import org.springframework.data.repository.CrudRepository;

public interface TipoPersonaCrudRepository extends CrudRepository<TipoPersona, Integer> {

}
