package com.sgl.ms.perfil.persistence.repository.perfil.control;

import com.sgl.ms.perfil.domain.dto.profile.control.ControlType;
import com.sgl.ms.perfil.domain.repository.profile.control.ControlTypeRepository;
import com.sgl.ms.perfil.persistence.crud.perfil.control.TipoControlCrudRepository;
import com.sgl.ms.perfil.persistence.entity.perfil.control.TipoControl;
import com.sgl.ms.perfil.persistence.mapper.perfil.control.ControlTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public class TipoControlRepository implements ControlTypeRepository {
    @Autowired
    private TipoControlCrudRepository tipoControlCrudRepository;
    @Autowired
    private ControlTypeMapper controlTypeMapper;

    @Override
    public List<ControlType> getAll() {
        List<TipoControl> tipoControles = (List<TipoControl>) tipoControlCrudRepository.findAll();
        return controlTypeMapper.toControlTypes(tipoControles);
    }

    @Override
    public ControlType save(ControlType controlType) {
        TipoControl tipoControl = controlTypeMapper.toTipoControl(controlType);
        return controlTypeMapper.toControlType(tipoControlCrudRepository.save(tipoControl));
    }

    @Override
    public Optional<ControlType> getControlTypeById(int id) {
        return tipoControlCrudRepository.findById(id).map(tipoControl -> controlTypeMapper.toControlType(tipoControl));
    }
}
