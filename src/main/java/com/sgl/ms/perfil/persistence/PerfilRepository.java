package com.sgl.ms.perfil.persistence;

import com.sgl.ms.perfil.domain.dto.Profile;
import com.sgl.ms.perfil.domain.repository.ProfileRepository;
import com.sgl.ms.perfil.persistence.crud.PerfilCrudRepository;
import com.sgl.ms.perfil.persistence.entity.Perfil;
import com.sgl.ms.perfil.persistence.mapper.ProfileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PerfilRepository implements ProfileRepository {
    @Autowired
    private PerfilCrudRepository perfilCrudRepository;
    @Autowired
    private ProfileMapper mapper;

    @Override
    public List<Profile> getAll() {
        List<Perfil> perfiles = (List<Perfil>) perfilCrudRepository.findAll();
        return mapper.toProfiles(perfiles);
    }

    @Override
    public List<Profile> getByDescription(String description) {
        List<Perfil> perfiles = perfilCrudRepository.findByDescripcion(description);
        return mapper.toProfiles(perfiles);
    }

    @Override
    public Optional<Profile> getById(int idProfile) {
        return perfilCrudRepository.findById(idProfile).map(perfil -> mapper.toProfile(perfil));
    }

    @Override
    public Profile save(Profile profile) {
        Perfil perfil = mapper.toPerfil(profile);
        return mapper.toProfile(perfilCrudRepository.save(perfil));
    }
    /*
    private PerfilCrudRepository perfilCrudRepository;

    public List<Perfil> getAll() {
        return (List<Perfil>) perfilCrudRepository.findAll();
    }

    public List<Perfil> getByDescripcion(String descripcion) {
        return perfilCrudRepository.findByDescripcion(descripcion);
    }

    public Optional<Perfil> getPerfil(int idPerfil) {
        return perfilCrudRepository.findById(idPerfil);
    }

    public Perfil save(Perfil perfil) {
        return perfilCrudRepository.save(perfil);
    }
     */
}
