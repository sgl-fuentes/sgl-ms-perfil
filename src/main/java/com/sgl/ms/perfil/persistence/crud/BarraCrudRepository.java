package com.sgl.ms.perfil.persistence.crud;

import com.sgl.ms.perfil.persistence.entity.Barra;
import com.sgl.ms.perfil.persistence.entity.Perfil;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BarraCrudRepository extends CrudRepository<Barra, Integer> {
    //List<Barra> findByTipo (String tipo);
    @Query(value = "select distinct barra.*, " +
            "opcion.id idOpcion, opcion.usuario_creacion_id usuarioCreacionIdOpcion, opcion.fecha_creacion fechaCreacionOpcion, " +
            "opcion.usuario_actualizacion_id usuarioActualizacionIdOpcion, opcion.fecha_actualizacion fechaActualizacionOpcion, " +
            "opcion.codigo codigoOpcion, opcion.descripcion descripcionOpcion, opcion.descripcion_corta descripcionCortaOpcion, " +
            "opcion.codigo_programa codigoProgramaOpcion, opcion.codigo_filtro codigoFiltroOpcion, " +
            "opcion.icono iconoOpcion, opcion.url urlOpcion, opcion.opcion_padre_id opcionPadreIdOpcion, opcion.orden ordenOpcion " +
            "from dev.opcion barra " +
            "inner join dev.opcion opcion " +
            "on barra.id = opcion.opcion_padre_id " +
            "where barra.nivel = :nivelBarra " +
            "order by barra.orden asc, opcion.orden asc ", nativeQuery=true)
    List<Barra> findAllTreeMenu(@Param(value = "nivelBarra") Integer nivelBarra);
    List<Barra> findByNivel (Integer nivel);
}