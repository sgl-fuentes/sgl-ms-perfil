package com.sgl.ms.perfil.persistence.mapper.perfil.opcion;

import com.sgl.ms.perfil.domain.dto.profile.option.CMenu;
import com.sgl.ms.perfil.persistence.entity.perfil.opcion.CBarra;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CMenuMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "descripcion", target = "description"),
            @Mapping(source = "descripcionCorta", target = "shortDescription"),
            @Mapping(source = "codigoPrograma", target = "programCode"),
            @Mapping(source = "codigoFiltro", target = "filterCode"),
            @Mapping(source = "icono", target = "icon"),
            @Mapping(source = "url", target = "uri"),
            @Mapping(source = "opcionPadreId", target = "fatherId"),
            @Mapping(source = "orden", target = "order"),
            @Mapping(source = "nivel", target = "level")
    })
    CMenu toCMenu(CBarra cBarra);
    List<CMenu> toCMenus(List<CBarra> cBarras);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "perfiles", ignore = true)
    })
    CBarra toCBarra(CMenu cMenu);
}
