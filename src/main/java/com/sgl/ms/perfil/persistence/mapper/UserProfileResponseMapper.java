package com.sgl.ms.perfil.persistence.mapper;

import com.sgl.ms.perfil.domain.dto.response.UserProfileResponse;
import com.sgl.ms.perfil.persistence.entity.UsuarioPerfil;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserProfileResponseMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "usuarioId", target = "userId"),
            @Mapping(source = "perfilId", target = "profileId")
    })
    UserProfileResponse toUserProfileResponse(UsuarioPerfil usuarioPerfil);
    List<UserProfileResponse> toUserProfileResponses(List<UsuarioPerfil> usuarioPerfiles);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "perfil", ignore = true),
            @Mapping(target = "usuario", ignore = true)
    })
    UsuarioPerfil toUsuarioPerfil(UserProfileResponse userProfileResponse);
}
