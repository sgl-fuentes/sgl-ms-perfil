package com.sgl.ms.perfil.persistence.repository.tabla;

import com.sgl.ms.perfil.domain.dto.table.Table;
import com.sgl.ms.perfil.domain.repository.table.TableRepository;
import com.sgl.ms.perfil.persistence.crud.tabla.TablaCrudRepository;
import com.sgl.ms.perfil.persistence.entity.tabla.Tabla;
import com.sgl.ms.perfil.persistence.mapper.table.TableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TablaRepository implements TableRepository {
    @Autowired
    private TablaCrudRepository tablaCrudRepository;
    @Autowired
    private TableMapper tableMapper;
    @Override
    public Optional<List<Table>> getByFatherIdIsNullAndOptionId(int opcionId) {
        Optional<List<Tabla>> tablas = tablaCrudRepository.findByTablaPadreIdIsNullAndOpcionId(opcionId);
        return Optional.of(tableMapper.toTables(tablas.get()));
    }
}
