package com.sgl.ms.perfil.persistence.crud.security;

import com.sgl.ms.perfil.persistence.entity.Security.UUsuario;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserCrudRepository extends CrudRepository<UUsuario, Integer> {
    Optional<UUsuario> findByCodigo (String codigo);
}
