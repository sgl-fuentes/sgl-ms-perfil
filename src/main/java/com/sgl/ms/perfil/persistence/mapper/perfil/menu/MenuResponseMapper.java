package com.sgl.ms.perfil.persistence.mapper.perfil.menu;

import com.sgl.ms.perfil.domain.dto.Menu;
import com.sgl.ms.perfil.domain.dto.Option;
import com.sgl.ms.perfil.domain.dto.response.MenuResponse;
import com.sgl.ms.perfil.domain.dto.response.OptionResponse;
import com.sgl.ms.perfil.persistence.entity.Barra;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {OptionResponseMapper.class})
public interface MenuResponseMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "descripcion", target = "label"),
            @Mapping(source = "icono", target = "icon"),
            @Mapping(source = "url", target = "path"),
            @Mapping(source = "orden", target = "order"),
            @Mapping(source = "opciones", target = "children"),
            @Mapping(target = "selected", ignore = true),
            @Mapping(target = "indeterminate", ignore = true),
    })
    MenuResponse toMenuResponse(Barra barra);
    List<MenuResponse> toMenuResponses(List<Barra> barras);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "usuarioCreacionId", ignore = true),
            @Mapping(target = "fechaCreacion", ignore = true),
            @Mapping(target = "usuarioActualizacionId", ignore = true),
            @Mapping(target = "fechaActualizacion", ignore = true),
            @Mapping(target = "codigo", ignore = true),
            @Mapping(target = "descripcion", ignore = true),
            @Mapping(target = "descripcionCorta", ignore = true),
            @Mapping(target = "codigoPrograma", ignore = true),
            @Mapping(target = "codigoFiltro", ignore = true),
            @Mapping(target = "opcionPadreId", ignore = true),
            @Mapping(target = "nivel", ignore = true)
    })
    Barra barra(MenuResponse menuResponse);

    /*@AfterMapping
    default void afterMapping(@MappingTarget MenuResponse menuResponse) {
        List<OptionResponse> sortedOptions = menuResponse.getChildren().stream().sorted((o1, o2) -> o1.getOrder() - o2.getOrder()).toList();
        menuResponse.setChildren(sortedOptions);
    };*/
}
