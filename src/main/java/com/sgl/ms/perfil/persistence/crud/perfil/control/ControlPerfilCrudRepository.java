package com.sgl.ms.perfil.persistence.crud.perfil.control;

import com.sgl.ms.perfil.persistence.entity.perfil.control.ControlPerfil;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ControlPerfilCrudRepository extends CrudRepository<ControlPerfil, Integer> {
    List<ControlPerfil> findByPerfilId(int idPerfil);
    List<ControlPerfil> findByControlIdAndPerfilId(int idControl, int idPerfil);
    Integer deleteByPerfilId(int perfilId);
}
