package com.sgl.ms.perfil.persistence.mapper;

import com.sgl.ms.perfil.domain.dto.Profile;
import com.sgl.ms.perfil.domain.dto.ProfileUser;
import com.sgl.ms.perfil.persistence.entity.Perfil;
import com.sgl.ms.perfil.persistence.entity.UsuarioPerfil;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
@Mapper(componentModel = "spring")
public interface ProfileUserMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "usuarioId", target = "userId"),
            @Mapping(source = "perfilId", target = "profileId")
    })
    ProfileUser toProfileUser(UsuarioPerfil usuarioPerfil);
    List<ProfileUser> toProfileUsers(List<UsuarioPerfil> usuarioPerfiles);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "usuario", ignore = true),
            @Mapping(target = "perfil", ignore = true)
    })
    UsuarioPerfil toUsuarioPerfil(ProfileUser profileUser);
}
