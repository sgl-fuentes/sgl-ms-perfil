package com.sgl.ms.perfil.persistence.mapper;

import com.sgl.ms.perfil.domain.dto.UserProfile;
import com.sgl.ms.perfil.persistence.entity.UsuarioPerfil;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface UserProfileMapper {
    @Mappings({
            @Mapping(source = "usuario", target = "user")
    })
    UserProfile toUserProfile(UsuarioPerfil usuarioPerfil);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "usuarioCreacionId", ignore = true),
            @Mapping(target = "fechaCreacion", ignore = true),
            @Mapping(target = "usuarioActualizacionId", ignore = true),
            @Mapping(target = "fechaActualizacion", ignore = true),
            @Mapping(target = "usuarioId", ignore = true),
            @Mapping(target = "perfilId", ignore = true),
            @Mapping(target = "perfil", ignore = true),
            @Mapping(target = "id", ignore = true)
            //@Mapping(target = "usuario", ignore = true)
            //@Mapping(target = "perfilId", ignore = true)
    })
    UsuarioPerfil toUsuarioPerfil(UserProfile userProfile);
}
