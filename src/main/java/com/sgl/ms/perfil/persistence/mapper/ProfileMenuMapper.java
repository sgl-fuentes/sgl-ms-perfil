package com.sgl.ms.perfil.persistence.mapper;

import com.sgl.ms.perfil.domain.dto.ProfileMenu;
import com.sgl.ms.perfil.persistence.entity.PerfilBarra;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
@Mapper(componentModel = "spring")
public interface ProfileMenuMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "barraId", target = "menuId"),
            @Mapping(source = "perfilId", target = "profileId"),
            @Mapping(source = "selected", target = "selected"),
            @Mapping(source = "indeterminate", target = "indeterminate")
    })
    ProfileMenu toProfileMenu(PerfilBarra perfilBarra);
    List<ProfileMenu> toProfileMenus(List<PerfilBarra> perfilBarras);
    @InheritInverseConfiguration
    PerfilBarra toPerfilBarra(ProfileMenu profileMenu);
}
