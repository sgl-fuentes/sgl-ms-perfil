package com.sgl.ms.perfil.persistence.mapper;

import com.sgl.ms.perfil.domain.dto.User;
import com.sgl.ms.perfil.persistence.entity.Usuario;
import com.sgl.ms.perfil.persistence.mapper.catalog.StatusMapper;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {StatusMapper.class})
public interface UserMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "clave", target = "password"),
            @Mapping(source = "apellidoPaterno", target = "lastName"),
            @Mapping(source = "apellidoMaterno", target = "secondLastName"),
            @Mapping(source = "primerNombre", target = "firstName"),
            @Mapping(source = "segundoNombre", target = "secondName"),
            @Mapping(source = "email", target = "email"),
            @Mapping(source = "personaId", target = "personId"),
            @Mapping(source = "nuevo", target = "novo"),
            @Mapping(source = "estadoId", target = "statusId"),
            @Mapping(source = "estado", target = "status"),
            @Mapping(target = "profile", ignore = true),
            @Mapping(target = "person", ignore = true)
    })
    User toUser(Usuario usuario);
    List<User> toUsers(List<Usuario> usuarios);
    @InheritInverseConfiguration
    Usuario toUsuario(User user);
}
