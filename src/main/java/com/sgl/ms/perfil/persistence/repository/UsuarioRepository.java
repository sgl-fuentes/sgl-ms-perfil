package com.sgl.ms.perfil.persistence.repository;

import com.sgl.ms.perfil.domain.dto.User;
import com.sgl.ms.perfil.domain.repository.UserRepository;
import com.sgl.ms.perfil.persistence.crud.UsuarioCrudRepository;
import com.sgl.ms.perfil.persistence.entity.Usuario;
import com.sgl.ms.perfil.persistence.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UsuarioRepository implements UserRepository  {

    @Autowired
    private UsuarioCrudRepository usuarioCrudRepository;
    @Autowired
    private UserMapper mapper;
    @Override
    public List<User> getAll() {
        List<Usuario> usuarios = (List<Usuario>) usuarioCrudRepository.findAll();
        return mapper.toUsers(usuarios);
    }

    @Override
    public List<User> getUserWithoutProfile() {
        List<Usuario> usuarios = (List<Usuario>) usuarioCrudRepository.getUserWithoutProfile();
        return mapper.toUsers(usuarios);
    }

    @Override
    public Optional<User> getById(int idUser) {
        return usuarioCrudRepository.findById(idUser).map(usuario -> mapper.toUser(usuario));
    }
    @Override
    public Optional<User> getByCodigo(String codigo) {
        return usuarioCrudRepository.findByCodigo(codigo).map(usuario -> mapper.toUser(usuario));
    }
    @Override
    public Optional<User> getByEmail(String email) {
        return usuarioCrudRepository.findByEmail(email).map(usuario -> mapper.toUser(usuario));
    }

    @Override
    public Optional<User> getByCodigoAndPassword(String codigo, String password) {
        return usuarioCrudRepository.findByCodigoAndClave(codigo, password).map(usuario -> mapper.toUser(usuario));
    }

    @Override
    public User save(User user) {
        Usuario usuario = mapper.toUsuario(user);
        return mapper.toUser(usuarioCrudRepository.save(usuario));
    }

    @Override
    public User update(User user) {
        Usuario usuario = mapper.toUsuario(user);
        return mapper.toUser(usuarioCrudRepository.save(usuario));
    }
}
