package com.sgl.ms.perfil.persistence.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "persona")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TipoPersona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "usuario_creacion_id")
    private Integer usuarioCreacionId;
    @Column(name = "fecha_creacion")
    private LocalDateTime fechaCreacion;
    @Column(name = "usuario_actualizacion_id")
    private Integer usuarioActualizacionId;
    @Column(name = "fecha_actualizacion")
    private LocalDateTime fechaActualizacion;
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "descripcion_corta")
    private String descripcionCorta;
    @Column(name = "codigo_programa")
    private String codigoPrograma;
    @Column(name = "codigo_filtro")
    private String codigoFiltro;
    @OneToMany(mappedBy = "tipoPersona")
    private List<PPersona> personas;
}
