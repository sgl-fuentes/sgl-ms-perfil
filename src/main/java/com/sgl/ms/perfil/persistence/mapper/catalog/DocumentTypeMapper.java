package com.sgl.ms.perfil.persistence.mapper.catalog;

import com.sgl.ms.perfil.domain.dto.catalog.DocumentType;
import com.sgl.ms.perfil.persistence.entity.catalog.maestro.TipoDocumento;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
@Mapper(componentModel = "spring")
public interface DocumentTypeMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "descripcion", target = "description"),
            @Mapping(source = "descripcionCorta", target = "shortDescription"),
            @Mapping(source = "codigoPrograma", target = "programCode"),
            @Mapping(source = "codigoFiltro", target = "filterCode")
    })
    DocumentType toDocumentType(TipoDocumento tipoDocumento);
    List<DocumentType> toDocumentTypes(List<TipoDocumento> tipoDocumentos);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "personaDetalles", ignore = true),
    })
    TipoDocumento toTipoDocumento(DocumentType documentType);
}
