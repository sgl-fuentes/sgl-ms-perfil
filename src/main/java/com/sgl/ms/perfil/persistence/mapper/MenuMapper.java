package com.sgl.ms.perfil.persistence.mapper;

import com.sgl.ms.perfil.domain.dto.Menu;
import com.sgl.ms.perfil.domain.dto.Option;
import com.sgl.ms.perfil.persistence.entity.Barra;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {OptionMapper.class})
public interface MenuMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "descripcion", target = "description"),
            @Mapping(source = "descripcionCorta", target = "shortDescription"),
            @Mapping(source = "codigoPrograma", target = "programCode"),
            @Mapping(source = "codigoFiltro", target = "filterCode"),
            @Mapping(source = "icono", target = "icon"),
            @Mapping(source = "url", target = "uri"),
            @Mapping(source = "opcionPadreId", target = "fatherId"),
            @Mapping(source = "orden", target = "order"),
            @Mapping(source = "nivel", target = "level"),
            @Mapping(source = "opciones", target = "options"),
    })
    Menu toMenu(Barra barra);
    List<Menu> toMenus(List<Barra> barras);
    @InheritInverseConfiguration
    Barra toBarra(Menu menu);

    /*@AfterMapping
    default void afterMapping(@MappingTarget Menu menu) {
        List<Option> sortedOptions = menu.getOptions().stream().sorted((o1, o2) -> o1.getOrder() - o2.getOrder()).toList();
        menu.setOptions(sortedOptions);
    };*/
}
