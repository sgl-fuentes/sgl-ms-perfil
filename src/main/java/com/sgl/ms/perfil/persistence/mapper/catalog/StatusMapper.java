package com.sgl.ms.perfil.persistence.mapper.catalog;

import com.sgl.ms.perfil.domain.dto.Status;
import com.sgl.ms.perfil.persistence.entity.catalog.maestro.Estado;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StatusMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "descripcion", target = "description"),
            @Mapping(source = "descripcionCorta", target = "shortDescription"),
            @Mapping(source = "codigoPrograma", target = "programCode"),
            @Mapping(source = "codigoFiltro", target = "filterCode")
    })
    Status toStatus(Estado estado);
    List<Status> toStatuses(List<Estado> estados);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "personas", ignore = true),
            @Mapping(target = "usuarios", ignore = true),
            @Mapping(target = "personaDetalles", ignore = true)
    })
    Estado toEstado(Status status);
}
