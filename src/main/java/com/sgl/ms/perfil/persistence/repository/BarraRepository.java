package com.sgl.ms.perfil.persistence.repository;

import com.sgl.ms.perfil.domain.dto.Menu;
import com.sgl.ms.perfil.domain.dto.response.MenuResponse;
import com.sgl.ms.perfil.domain.repository.MenuRepository;
import com.sgl.ms.perfil.persistence.crud.BarraCrudRepository;
import com.sgl.ms.perfil.persistence.crud.PerfilCrudRepository;
import com.sgl.ms.perfil.persistence.entity.Barra;
import com.sgl.ms.perfil.persistence.entity.Perfil;
import com.sgl.ms.perfil.persistence.entity.Producto;
import com.sgl.ms.perfil.persistence.mapper.MenuMapper;
import com.sgl.ms.perfil.persistence.mapper.ProfileMapper;
import com.sgl.ms.perfil.persistence.mapper.perfil.menu.MenuResponseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class BarraRepository implements MenuRepository {
    @Autowired
    private BarraCrudRepository barraCrudRepository;
    @Autowired
    private MenuMapper mapper;
    @Autowired
    private MenuResponseMapper responseMapper;

    /*
    @Override
    public List<Menu> getAll() {
        List<Barra> barras = (List<Barra>) barraCrudRepository.findAll();
        return mapper.toMenus(barras);
    }

    @Override
    public List<Menu> getAllCollapsable() {
        List<Barra> barras = (List<Barra>) barraCrudRepository.findByTipo("collapsable");
        return mapper.toMenus(barras);
    }

    @Override
    public List<Menu> findAllMenu(String menuType, String optionType) {
        List<Barra> barras = (List<Barra>) barraCrudRepository.findAllMenu(menuType, optionType);
        return mapper.toMenus(barras);
    }
    */

    @Override
    public List<MenuResponse> findAllTreeMenu(Integer menuLevel) {
        List<Barra> barras = (List<Barra>) barraCrudRepository.findAllTreeMenu(menuLevel);
        return responseMapper.toMenuResponses(barras);
    }

    @Override
    public List<MenuResponse> findByLevel(Integer menuLevel) {
        List<Barra> barras = barraCrudRepository.findByNivel(menuLevel);
        return responseMapper.toMenuResponses(barras);
    }

    @Override
    public Menu save(Menu menu) {
        Barra barra = mapper.toBarra(menu);
        return mapper.toMenu(barraCrudRepository.save(barra));
    }

    @Override
    public Optional<Menu> getMenu(int menuId) {
        return barraCrudRepository.findById(menuId).map(barra -> mapper.toMenu(barra));
    }

    @Override
    public void delete(int menuId) {
        barraCrudRepository.deleteById(menuId);
    }
}
