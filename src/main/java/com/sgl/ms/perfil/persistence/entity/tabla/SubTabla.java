package com.sgl.ms.perfil.persistence.entity.tabla;

import com.sgl.ms.perfil.persistence.entity.Categoria;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
@Entity
@Table(name = "tabla")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubTabla {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "usuario_creacion_id")
    private Integer usuarioCreacionId;
    @Column(name = "fecha_creacion")
    private LocalDateTime fechaCreacion;
    @Column(name = "usuario_actualizacion_id")
    private Integer usuarioActualizacionId;
    @Column(name = "fecha_actualizacion")
    private LocalDateTime fechaActualizacion;
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "descripcion_corta")
    private String descripcionCorta;
    @Column(name = "codigo_programa")
    private String codigoPrograma;
    @Column(name = "codigo_filtro")
    private String codigoFiltro;
    @Column(name = "opcion_id")
    private Integer opcionId;
    @Column(name = "icono")
    private String icono;
    @Column(name = "url")
    private String url;
    @Column(name = "tabla_padre_id")
    private Integer tablaPadreId;
    @Column(name = "orden")
    private Integer orden;
    @Column(name = "tipo_tabla_id")
    private Integer tipoTablaId;
    @Column(name = "nivel")
    private Integer nivel;
    @ManyToOne
    @JoinColumn(name = "tabla_padre_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Tabla tabla;
}
