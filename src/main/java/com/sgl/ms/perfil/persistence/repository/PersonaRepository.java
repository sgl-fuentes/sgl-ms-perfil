package com.sgl.ms.perfil.persistence.repository;

import com.sgl.ms.perfil.domain.dto.Person;
import com.sgl.ms.perfil.domain.repository.PersonRepository;
import com.sgl.ms.perfil.persistence.crud.PersonaCrudRepository;
import com.sgl.ms.perfil.persistence.entity.PPersona;
import com.sgl.ms.perfil.persistence.mapper.PersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PersonaRepository implements PersonRepository {
    @Autowired
    private PersonaCrudRepository personaCrudRepository;
    @Autowired
    private PersonMapper personMapper;

    @Override
    public List<Person> getAll() {
        List<PPersona> personas = (List<PPersona>) personaCrudRepository.findAll();
        return personMapper.toPersons(personas);
    }

    @Override
    public Person save(Person person) {
        PPersona persona = personMapper.toPersona(person);
        return personMapper.toPerson(personaCrudRepository.save(persona));
    }

    @Override
    public Optional<Person> getById(int personId) {
        return personaCrudRepository.findById(personId).map(persona -> personMapper.toPerson(persona));
    }

    @Override
    public boolean delete(int personId) {
        if(this.getById(personId).get() != null){
            personaCrudRepository.deleteById(personId);
            return true;
        }
        else{
            return false;
        }
    }
    @Override
    public Optional<List<Person>> findByTipoDocumentoIdentidadIdAndDocumentoIdentidad(int tipoDoc, String nroDoc) {
        return Optional.of(personMapper.toPersons(personaCrudRepository.findByTipoDocumentoIdentidadIdAndDocumentoIdentidad(tipoDoc, nroDoc).get()));
    }
}
