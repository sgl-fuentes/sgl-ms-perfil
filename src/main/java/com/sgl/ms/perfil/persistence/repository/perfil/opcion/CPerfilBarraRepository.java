package com.sgl.ms.perfil.persistence.repository.perfil.opcion;

import com.sgl.ms.perfil.domain.dto.profile.option.CProfileMenu;
import com.sgl.ms.perfil.domain.repository.profile.option.CProfileMenuRepository;
import com.sgl.ms.perfil.persistence.crud.perfil.opcion.CPerfilBarraCrudRepository;
import com.sgl.ms.perfil.persistence.entity.Producto;
import com.sgl.ms.perfil.persistence.entity.perfil.opcion.CPerfilBarra;
import com.sgl.ms.perfil.persistence.mapper.perfil.opcion.CProfileMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class CPerfilBarraRepository implements CProfileMenuRepository {
    @Autowired
    private CProfileMenuMapper cProfileMenuMapper;
    @Autowired
    private CPerfilBarraCrudRepository cPerfilBarraCrudRepository;
    @Override
    public List<CProfileMenu> getByProfileId(int profileId) {
        return cProfileMenuMapper.toCProfileMenus(cPerfilBarraCrudRepository.findByPerfilId(profileId));
    }
    @Override
    public CProfileMenu findByPerfilIdAndOpcionId(int idPerfil, int idOpcion) {
        return cProfileMenuMapper.toCProfileMenu(cPerfilBarraCrudRepository.findByPerfilIdAndBarraId(idPerfil, idOpcion));
    }
    @Override
    public CProfileMenu save(CProfileMenu cProfileMenu) {
        CPerfilBarra cPerfilBarra = cProfileMenuMapper.toCPerfilBarra(cProfileMenu);
        return cProfileMenuMapper.toCProfileMenu(cPerfilBarraCrudRepository.save(cPerfilBarra));
    }
    @Override
    public Integer deleteByProfileId(int profileId) {
        Integer result = cPerfilBarraCrudRepository.deleteByPerfilId(profileId);
        return result;
    }
}
