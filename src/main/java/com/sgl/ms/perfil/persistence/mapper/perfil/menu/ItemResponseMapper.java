package com.sgl.ms.perfil.persistence.mapper.perfil.menu;

import com.sgl.ms.perfil.domain.dto.response.ItemResponse;
import com.sgl.ms.perfil.persistence.entity.SubOpcion;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ItemResponseMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "descripcion", target = "label"),
            @Mapping(source = "icono", target = "icon"),
            @Mapping(source = "url", target = "path"),
            @Mapping(source = "orden", target = "order"),
            @Mapping(target = "selected", ignore = true),
            @Mapping(target = "indeterminate", ignore = true),
            @Mapping(target = "children", ignore = true)
    })
    ItemResponse toItemResponse(SubOpcion subOpcion);
    List<ItemResponse> toItemResponses(List<SubOpcion> subOpcion);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "opcion", ignore = true),
            @Mapping(target = "usuarioCreacionId", ignore = true),
            @Mapping(target = "fechaCreacion", ignore = true),
            @Mapping(target = "usuarioActualizacionId", ignore = true),
            @Mapping(target = "fechaActualizacion", ignore = true),
            @Mapping(target = "codigo", ignore = true),
            @Mapping(target = "descripcion", ignore = true),
            @Mapping(target = "descripcionCorta", ignore = true),
            @Mapping(target = "codigoPrograma", ignore = true),
            @Mapping(target = "codigoFiltro", ignore = true),
            @Mapping(target = "opcionPadreId", ignore = true),
            @Mapping(target = "nivel", ignore = true)
    })
    SubOpcion subOpcion(ItemResponse itemResponse);
}
