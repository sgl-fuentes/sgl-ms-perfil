package com.sgl.ms.perfil.persistence.repository.perfil.control;

import com.sgl.ms.perfil.domain.dto.profile.control.CControlProfile;
import com.sgl.ms.perfil.domain.repository.profile.control.CControlProfileRepository;
import com.sgl.ms.perfil.persistence.crud.perfil.control.ControlPerfilCrudRepository;
import com.sgl.ms.perfil.persistence.entity.perfil.control.ControlPerfil;
import com.sgl.ms.perfil.persistence.mapper.perfil.control.ControlProfileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ControlPerfilRepository implements CControlProfileRepository {

    @Autowired
    private ControlPerfilCrudRepository controlPerfilCrudRepository;
    @Autowired
    private ControlProfileMapper controlProfileMapper;

    @Override
    public List<CControlProfile> getAll() {
        List<ControlPerfil> controlPerfiles = (List<ControlPerfil>) controlPerfilCrudRepository.findAll();
        return controlProfileMapper.toCControlProfiles(controlPerfiles);
    }

    @Override
    public CControlProfile save(CControlProfile cControlProfile) {
        ControlPerfil controlPerfil = controlProfileMapper.toControlPerfil(cControlProfile);
        return controlProfileMapper.toCControlProfile(controlPerfilCrudRepository.save(controlPerfil));
    }

    @Override
    public Optional<List<CControlProfile>> getByPerfilId(int perfilId) {
        return Optional.of(controlProfileMapper.toCControlProfiles(controlPerfilCrudRepository.findByPerfilId(perfilId)));
    }

    @Override
    public Optional<List<CControlProfile>> getByControlIdAndPerfilId(int controlId, int profileId) {
        return Optional.of(controlProfileMapper.toCControlProfiles(controlPerfilCrudRepository.findByControlIdAndPerfilId(controlId, profileId)));
    }

    @Override
    public Integer deleteByProfileId(int profileId) {
        Integer result = controlPerfilCrudRepository.deleteByPerfilId(profileId);
        return result;
    }

    @Override
    public Integer deleteById(int id) {
        if(controlPerfilCrudRepository.findById(id) != null){
            controlPerfilCrudRepository.deleteById(id);
            return 1;
        }
        else{
            return 0;
        }
    }
}
