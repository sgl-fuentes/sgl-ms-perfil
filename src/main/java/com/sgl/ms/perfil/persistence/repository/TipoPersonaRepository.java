package com.sgl.ms.perfil.persistence.repository;

import com.sgl.ms.perfil.domain.dto.PersonType;
import com.sgl.ms.perfil.domain.repository.PersonTypeRepository;
import com.sgl.ms.perfil.persistence.crud.TipoPersonaCrudRepository;
import com.sgl.ms.perfil.persistence.entity.TipoPersona;
import com.sgl.ms.perfil.persistence.mapper.PersonTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public class TipoPersonaRepository implements PersonTypeRepository {

    @Autowired
    private TipoPersonaCrudRepository tipoPersonaCrudRepository;
    @Autowired
    private PersonTypeMapper personTypeMapper;

    @Override
    public List<PersonType> getAll() {
        List<TipoPersona> tipoPersonas = (List<TipoPersona>) tipoPersonaCrudRepository.findAll();
        return personTypeMapper.toPersonTypes(tipoPersonas);
    }

    @Override
    public Optional<PersonType> getById(int personTypeId) {
        return tipoPersonaCrudRepository.findById(personTypeId).map(personType -> personTypeMapper.toPersonType(personType));
    }

    @Override
    public PersonType save(PersonType personType) {
        TipoPersona tipoPersona = personTypeMapper.toTipoPersona(personType);
        return personTypeMapper.toPersonType(tipoPersonaCrudRepository.save(tipoPersona));
    }

    @Override
    public boolean delete(int personTypeId) {
        if(this.getById(personTypeId).get() != null){
            tipoPersonaCrudRepository.deleteById(personTypeId);
            return true;
        }
        else{
            return false;
        }
    }
}
