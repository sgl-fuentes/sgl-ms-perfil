package com.sgl.ms.perfil.persistence.mapper;

import com.sgl.ms.perfil.domain.dto.PersonDetail;
import com.sgl.ms.perfil.persistence.entity.PersonaDetalle;
import com.sgl.ms.perfil.persistence.mapper.catalog.DocumentTypeMapper;
import com.sgl.ms.perfil.persistence.mapper.catalog.StatusMapper;
import com.sgl.ms.perfil.persistence.mapper.perfil.opcion.CMenuMapper;
import com.sgl.ms.perfil.persistence.mapper.perfil.opcion.CProfileMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
@Mapper(componentModel = "spring", uses = {PersonMapper.class, DocumentTypeMapper.class, StatusMapper.class})
public interface PersonDetailMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "personaId", target = "personId"),
            @Mapping(source = "tipoDocumentoId", target = "documentTypeId"),
            //@Mapping(source = "imagen", target = "image"),
            @Mapping(source = "fechaVencimiento", target = "dueDate"),
            @Mapping(source = "tipoDetalleId", target = "detailTypeId"),
            @Mapping(source = "empresaId", target = "companyId"),
            @Mapping(source = "cuenta", target = "account"),
            @Mapping(source = "cuentaCci", target = "cciAccount"),
            @Mapping(source = "estadoId", target = "statusId"),
            @Mapping(source = "persona", target = "person"),
            @Mapping(source = "tipoDocumento", target = "documentType"),
            @Mapping(source = "estado", target = "status"),
            @Mapping(target = "image", ignore = true)

    })
    PersonDetail toPersonDetail(PersonaDetalle personaDetalle);
    List<PersonDetail> toPersonDetails(List<PersonaDetalle> personaDetalles);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "imagen", ignore = true)
    })
    PersonaDetalle toPersonaDetalle(PersonDetail personDetail);
}
