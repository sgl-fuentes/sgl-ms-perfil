package com.sgl.ms.perfil.persistence.crud;

import com.sgl.ms.perfil.persistence.entity.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UsuarioCrudRepository extends CrudRepository<Usuario, Integer> {
    Optional<Usuario> findByCodigo(String codigo);
    Optional<Usuario> findByEmail(String email);
    Optional<Usuario> findByCodigoAndClave(String codigo, String clave);
    @Query(value = "SELECT u.id, u.usuario_creacion_id, u.fecha_creacion, " +
            "u.usuario_actualizacion_id, u.fecha_actualizacion, u.codigo, " +
            "u.clave, u.apellido_paterno, u.apellido_materno, " +
            "u.primer_nombre, u.segundo_nombre, u.email, " +
            "u.persona_id, u.nuevo, u.estado_id " +
            "FROM " +
            "dev.usuario u " +
            "left join " +
            "dev.usuario_perfil up " +
            "on u.id = up.usuario_id " +
            "where up.id is null", nativeQuery = true)
    List<Usuario> getUserWithoutProfile();
}
