package com.sgl.ms.perfil.persistence.crud;

import com.sgl.ms.perfil.persistence.entity.Producto;
import com.sgl.ms.perfil.persistence.entity.UsuarioPerfil;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuarioPerfilCrudRepository extends CrudRepository<UsuarioPerfil, Integer> {
    List<UsuarioPerfil> findByUsuarioId(int idUsuario);
    UsuarioPerfil findByUsuarioIdAndPerfilId(int usuarioId, int perfilId);
}
