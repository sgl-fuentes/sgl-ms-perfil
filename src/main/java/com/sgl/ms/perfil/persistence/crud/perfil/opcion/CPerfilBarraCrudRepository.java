package com.sgl.ms.perfil.persistence.crud.perfil.opcion;

import com.sgl.ms.perfil.persistence.entity.Producto;
import com.sgl.ms.perfil.persistence.entity.perfil.opcion.CPerfilBarra;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CPerfilBarraCrudRepository extends CrudRepository<CPerfilBarra, Integer> {
    List<CPerfilBarra> findByPerfilId(int idPerfil);
    CPerfilBarra findByPerfilIdAndBarraId(int idPerfil, int idOpcion);
    Integer deleteByPerfilId(int perfilId);
}
