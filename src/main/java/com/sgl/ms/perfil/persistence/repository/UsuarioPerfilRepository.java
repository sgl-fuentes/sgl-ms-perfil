package com.sgl.ms.perfil.persistence.repository;

import com.sgl.ms.perfil.domain.dto.ProfileUser;
import com.sgl.ms.perfil.domain.dto.Purchase;
import com.sgl.ms.perfil.domain.dto.UserProfile;
import com.sgl.ms.perfil.domain.dto.response.UserProfileResponse;
import com.sgl.ms.perfil.domain.repository.UserProfileRepository;
import com.sgl.ms.perfil.persistence.crud.UsuarioPerfilCrudRepository;
import com.sgl.ms.perfil.persistence.entity.Compra;
import com.sgl.ms.perfil.persistence.entity.UsuarioPerfil;
import com.sgl.ms.perfil.persistence.mapper.ProfileUserMapper;
import com.sgl.ms.perfil.persistence.mapper.UserProfileMapper;
import com.sgl.ms.perfil.persistence.mapper.UserProfileResponseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UsuarioPerfilRepository implements UserProfileRepository {
    @Autowired
    private UsuarioPerfilCrudRepository usuarioPerfilCrudRepository;
    @Autowired
    private ProfileUserMapper profileUserMapper;
    @Autowired
    private UserProfileResponseMapper userProfileResponseMapper;
    @Override
    public ProfileUser save(ProfileUser profileUser) {
        UsuarioPerfil usuarioPerfil = profileUserMapper.toUsuarioPerfil(profileUser);
        return profileUserMapper.toProfileUser(usuarioPerfilCrudRepository.save(usuarioPerfil));
    }
    @Override
    public List<UserProfileResponse> findByUsuarioId(int idUsuario) {
        List<UsuarioPerfil> usuarioPerfiles = usuarioPerfilCrudRepository.findByUsuarioId(idUsuario);
        return userProfileResponseMapper.toUserProfileResponses(usuarioPerfiles);
    }
    @Override
    public void deleteById(int id) {
        usuarioPerfilCrudRepository.deleteById(id);
    }
    @Override
    public ProfileUser findById(int userProfileId) {
        UsuarioPerfil usuarioPerfil = usuarioPerfilCrudRepository.findById(userProfileId).get();
        return profileUserMapper.toProfileUser(usuarioPerfil);
    }
    @Override
    public UserProfileResponse getByUserIdAndProfileId(int userId, int profileId) {
        UsuarioPerfil usuarioPerfil = usuarioPerfilCrudRepository.findByUsuarioIdAndPerfilId(userId, profileId);
        return userProfileResponseMapper.toUserProfileResponse(usuarioPerfil);
    }
}
