package com.sgl.ms.perfil.persistence.repository;

import com.sgl.ms.perfil.domain.dto.Item;
import com.sgl.ms.perfil.domain.repository.ItemRepository;
import com.sgl.ms.perfil.persistence.crud.SubOpcionCrudRepository;
import com.sgl.ms.perfil.persistence.entity.SubOpcion;
import com.sgl.ms.perfil.persistence.mapper.ItemMapper;
import com.sgl.ms.perfil.persistence.mapper.perfil.menu.ItemResponseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SubOpcionRepository implements ItemRepository {
    @Autowired
    private SubOpcionCrudRepository subOpcionCrudRepository;
    @Autowired
    private ItemMapper mapper;
    @Autowired
    private ItemResponseMapper responseMapper;

    @Override
    public Item save(Item item) {
        SubOpcion subOpcion = mapper.toSubOpcion(item);
        return mapper.toItem(subOpcionCrudRepository.save(subOpcion));
    }
}
