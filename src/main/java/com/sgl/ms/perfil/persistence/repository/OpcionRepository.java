package com.sgl.ms.perfil.persistence.repository;

import com.sgl.ms.perfil.domain.dto.Option;
import com.sgl.ms.perfil.domain.repository.OptionRepository;
import com.sgl.ms.perfil.persistence.crud.OpcionCrudRepository;
import com.sgl.ms.perfil.persistence.entity.Opcion;
import com.sgl.ms.perfil.persistence.mapper.OptionMapper;
import com.sgl.ms.perfil.persistence.mapper.perfil.menu.OptionResponseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OpcionRepository implements OptionRepository {
    @Autowired
    private OpcionCrudRepository opcionCrudRepository;
    @Autowired
    private OptionMapper mapper;
    @Autowired
    private OptionResponseMapper responseMapper;

    @Override
    public Option save(Option option) {
        Opcion opcion = mapper.toOpcion(option);
        return mapper.toOption(opcionCrudRepository.save(opcion));
    }
}
