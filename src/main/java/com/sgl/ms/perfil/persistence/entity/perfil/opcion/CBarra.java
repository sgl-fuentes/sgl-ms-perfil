package com.sgl.ms.perfil.persistence.entity.perfil.opcion;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "opcion")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CBarra {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "usuario_creacion_id")
    private Integer usuarioCreacionId;
    @Column(name = "fecha_creacion")
    private LocalDateTime fechaCreacion;
    @Column(name = "usuario_actualizacion_id")
    private Integer usuarioActualizacionId;
    @Column(name = "fecha_actualizacion")
    private LocalDateTime fechaActualizacion;
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "descripcion_corta")
    private String descripcionCorta;
    @Column(name = "codigo_programa")
    private String codigoPrograma;
    @Column(name = "codigo_filtro")
    private String codigoFiltro;
    @Column(name = "icono")
    private String icono;
    @Column(name = "url")
    private String url;
    @Column(name = "opcion_padre_id")
    private Integer opcionPadreId;
    @Column(name = "orden")
    private Integer orden;
    @Column(name = "nivel")
    private Integer nivel;
    @OneToMany(mappedBy = "cBarra", cascade = {CascadeType.ALL})
    private List<CPerfilBarra> perfiles;
}
