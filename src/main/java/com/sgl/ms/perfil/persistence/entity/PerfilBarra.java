package com.sgl.ms.perfil.persistence.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "opcion_perfil")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PerfilBarra {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "usuario_creacion_id")
    private Integer usuarioCreacionId;

    @Column(name = "fecha_creacion")
    private LocalDateTime fechaCreacion;

    @Column(name = "usuario_actualizacion_id")
    private Integer usuarioActualizacionId;

    @Column(name = "fecha_actualizacion")
    private LocalDateTime fechaActualizacion;

    @Column(name = "opcion_id")
    private Integer barraId;

    @Column(name = "perfil_id")
    private Integer perfilId;

    @Column(name = "selected")
    private Boolean selected;

    @Column(name = "indeterminate")
    private Boolean indeterminate;
}
