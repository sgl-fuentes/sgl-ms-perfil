package com.sgl.ms.perfil.persistence.mapper.perfil.control;

import com.sgl.ms.perfil.domain.dto.profile.control.CControl;
import com.sgl.ms.perfil.persistence.entity.perfil.control.Control;
import com.sgl.ms.perfil.persistence.mapper.OptionMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring",uses = {ControlTypeMapper.class, OptionMapper.class})
public interface ControlMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "descripcion", target = "description"),
            @Mapping(source = "descripcionCorta", target = "shortDescription"),
            @Mapping(source = "codigoPrograma", target = "programCode"),
            @Mapping(source = "codigoFiltro", target = "filterCode"),
            @Mapping(source = "opcionId", target = "optionId"),
            @Mapping(source = "control", target = "control"),
            @Mapping(source = "html", target = "html"),
            @Mapping(source = "tipoControlId", target = "controlTypeId"),
            @Mapping(source = "tipoControl", target = "controlType"),
            @Mapping(source = "opcion", target = "option")
    })
    CControl toCControl(Control control);
    List<CControl> toCControls(List<Control> controles);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "controlPerfiles", ignore = true),
    })
    Control toControl(CControl cControl);
}
