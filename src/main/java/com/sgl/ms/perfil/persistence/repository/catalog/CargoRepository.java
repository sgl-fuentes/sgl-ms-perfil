package com.sgl.ms.perfil.persistence.repository.catalog;

import com.sgl.ms.perfil.domain.dto.catalog.WorkPosition;
import com.sgl.ms.perfil.domain.repository.catalog.WorkPositionRepository;
import com.sgl.ms.perfil.persistence.crud.catalog.CargoCrudRepository;
import com.sgl.ms.perfil.persistence.entity.catalog.maestro.Cargo;
import com.sgl.ms.perfil.persistence.entity.catalog.maestro.TipoDocumentoIdentidad;
import com.sgl.ms.perfil.persistence.mapper.catalog.WorkPositionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CargoRepository implements WorkPositionRepository {
    @Autowired
    private CargoCrudRepository cargoCrudRepository;
    @Autowired
    private WorkPositionMapper workPositionMapper;

    @Override
    public List<WorkPosition> getAll() {
        List<Cargo> cargos = (List<Cargo>) cargoCrudRepository.findAll();
        return workPositionMapper.toWorkPositions(cargos);
    }

    @Override
    public WorkPosition save(WorkPosition workPosition) {
        Cargo cargo = workPositionMapper.toCargo(workPosition);
        return workPositionMapper.toWorkPosition(cargoCrudRepository.save(cargo));
    }

    @Override
    public void delete(int workPositionId) {
        cargoCrudRepository.deleteById(workPositionId);
    }

    @Override
    public Optional<WorkPosition> getById(int id) {
        return cargoCrudRepository.findById(id).map(i -> workPositionMapper.toWorkPosition(i));
    }
}
