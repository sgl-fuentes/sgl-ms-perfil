package com.sgl.ms.perfil.persistence.mapper.catalog;

import com.sgl.ms.perfil.domain.dto.catalog.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface CatalogMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "userCreationId", target = "userCreationId"),
            @Mapping(source = "creationDate", target = "creationDate"),
            @Mapping(source = "userModificationId", target = "userModificationId"),
            @Mapping(source = "modificationDate", target = "modificationDate"),
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "description", target = "description"),
            @Mapping(source = "shortDescription", target = "shortDescription"),
            @Mapping(source = "programCode", target = "programCode"),
            @Mapping(source = "filterCode", target = "filterCode")
    })
    DocumentType toDocumentType(Catalog catalog);
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "userCreationId", target = "userCreationId"),
            @Mapping(source = "creationDate", target = "creationDate"),
            @Mapping(source = "userModificationId", target = "userModificationId"),
            @Mapping(source = "modificationDate", target = "modificationDate"),
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "description", target = "description"),
            @Mapping(source = "shortDescription", target = "shortDescription"),
            @Mapping(source = "programCode", target = "programCode"),
            @Mapping(source = "filterCode", target = "filterCode")
    })
    Group toGroup(Catalog catalog);
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "userCreationId", target = "userCreationId"),
            @Mapping(source = "creationDate", target = "creationDate"),
            @Mapping(source = "userModificationId", target = "userModificationId"),
            @Mapping(source = "modificationDate", target = "modificationDate"),
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "description", target = "description"),
            @Mapping(source = "shortDescription", target = "shortDescription"),
            @Mapping(source = "programCode", target = "programCode"),
            @Mapping(source = "filterCode", target = "filterCode")
    })
    IdentityDocumentType toIdentityDocumentType(Catalog catalog);
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "userCreationId", target = "userCreationId"),
            @Mapping(source = "creationDate", target = "creationDate"),
            @Mapping(source = "userModificationId", target = "userModificationId"),
            @Mapping(source = "modificationDate", target = "modificationDate"),
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "description", target = "description"),
            @Mapping(source = "shortDescription", target = "shortDescription"),
            @Mapping(source = "programCode", target = "programCode"),
            @Mapping(source = "filterCode", target = "filterCode")
    })
    WorkPosition toWorkPosition(Catalog catalog);
}
