package com.sgl.ms.perfil.persistence.entity.perfil.control;

import com.sgl.ms.perfil.persistence.entity.Perfil;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "control_perfil")
@Getter
@Setter
@NoArgsConstructor
public class ControlPerfil {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "usuario_creacion_id")
    private Integer usuarioCreacionId;
    @Column(name = "fecha_creacion")
    private LocalDateTime fechaCreacion;
    @Column(name = "usuario_actualizacion_id")
    private Integer usuarioActualizacionId;
    @Column(name = "fecha_actualizacion")
    private LocalDateTime fechaActualizacion;
    @Column(name = "control_id")
    private Integer controlId;
    @Column(name = "perfil_id")
    private Integer perfilId;
    @Column(name = "flag")
    private Boolean flag;
    @ManyToOne
    @JoinColumn(name = "control_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Control control;
    @ManyToOne
    @JoinColumn(name = "perfil_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Perfil perfil;
}
