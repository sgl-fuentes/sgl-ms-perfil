package com.sgl.ms.perfil.persistence.mapper.perfil.opcion;

import com.sgl.ms.perfil.domain.dto.profile.option.CProfile;
import com.sgl.ms.perfil.persistence.entity.perfil.opcion.CPerfil;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CProfileMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "usuarioCreacionId", target = "userCreationId"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioActualizacionId", target = "userModificationId"),
            @Mapping(source = "fechaActualizacion", target = "modificationDate"),
            @Mapping(source = "codigo", target = "code"),
            @Mapping(source = "descripcion", target = "description"),
            @Mapping(source = "descripcionCorta", target = "shortDescription"),
            @Mapping(source = "codigoPrograma", target = "programCode"),
            @Mapping(source = "codigoFiltro", target = "filterCode")
    })
    CProfile toCProfile(CPerfil cPerfil);
    List<CProfile> toCProfiles(List<CPerfil> cPerfiles);
    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "barras", ignore = true)
    })
    CPerfil toCPerfil(CProfile cProfile);
}
