package com.sgl.ms.perfil.persistence.repository;

import com.sgl.ms.perfil.domain.dto.PersonDetail;
import com.sgl.ms.perfil.domain.dto.request.PersonDetailRequest;
import com.sgl.ms.perfil.domain.repository.PersonDetailRepository;
import com.sgl.ms.perfil.persistence.crud.PersonaDetalleCrudRepository;
import com.sgl.ms.perfil.persistence.entity.PersonaDetalle;
import com.sgl.ms.perfil.persistence.mapper.PersonDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class PersonaDetalleRepository implements PersonDetailRepository {
    @Autowired
    private PersonaDetalleCrudRepository personaDetalleCrudRepository;
    @Autowired
    private PersonDetailMapper personDetailMapper;
    @Override
    public List<PersonDetail> getAll() {
        List<PersonaDetalle> personaDetalles = (List<PersonaDetalle>) personaDetalleCrudRepository.findAll();
        return personDetailMapper.toPersonDetails(personaDetalles);
    }

    @Override
    public PersonDetail save(PersonDetail personDetail) {
        PersonaDetalle personaDetalle = personDetailMapper.toPersonaDetalle(personDetail);
        return personDetailMapper.toPersonDetail(personaDetalleCrudRepository.save(personaDetalle));
    }

    @Override
    public Optional<List<PersonDetail>> getPersonDetailByPersonaId(int personId) {
        List<PersonaDetalle> personaDetalles = personaDetalleCrudRepository.findByPersonaId(personId).get();
        List<PersonDetail> personDetailResponse = new ArrayList<>();
        for(PersonaDetalle pd : personaDetalles){
            PersonDetail personDetail = personDetailMapper.toPersonDetail(pd);
            if(pd.getImagen() != null){
                personDetail.setImage(new String(pd.getImagen(), StandardCharsets.UTF_8));
            }
            personDetailResponse.add(personDetail);
        }
        return Optional.of(personDetailResponse);
        //return personaDetalleCrudRepository.findByPersonaId(personId).map(personaDetalles -> personDetailMapper.toPersonDetails(personaDetalles));
    }

    @Override
    public void delete(int personDetailId) {
        personaDetalleCrudRepository.deleteById(personDetailId);
    }

    @Override
    public Optional<PersonDetail> getById(int id) {
        PersonaDetalle personaDetalle = personaDetalleCrudRepository.findById(id).get();
        return Optional.of(personDetailMapper.toPersonDetail(personaDetalle));
    }

    @Override
    public List<PersonDetail> saveAll(List<PersonDetail> personDetails) {
        List<PersonDetail> pDetailResponse = new ArrayList<>();
        for(PersonDetail pd : personDetails){
            PersonDetail personDetail = new PersonDetail();
            PersonaDetalle pdetalle = personDetailMapper.toPersonaDetalle(pd);
            if(pd.getImage() != null){
                pdetalle.setImagen(pd.getImage().getBytes());
            }
            personDetail = personDetailMapper.toPersonDetail(personaDetalleCrudRepository.save(pdetalle));
            pDetailResponse.add(personDetail);
        }
        return pDetailResponse;
    }
}
