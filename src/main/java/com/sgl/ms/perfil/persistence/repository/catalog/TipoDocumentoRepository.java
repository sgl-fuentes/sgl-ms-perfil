package com.sgl.ms.perfil.persistence.repository.catalog;

import com.sgl.ms.perfil.domain.dto.catalog.DocumentType;
import com.sgl.ms.perfil.domain.repository.catalog.DocumentTypeRepository;
import com.sgl.ms.perfil.persistence.crud.catalog.TipoDocumentoCrudRepository;
import com.sgl.ms.perfil.persistence.entity.Barra;
import com.sgl.ms.perfil.persistence.entity.catalog.maestro.TipoDocumento;
import com.sgl.ms.perfil.persistence.mapper.catalog.DocumentTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TipoDocumentoRepository implements DocumentTypeRepository {
    @Autowired
    private TipoDocumentoCrudRepository tipoDocumentoCrudRepository;
    @Autowired
    private DocumentTypeMapper documentTypeMapper;

    @Override
    public List<DocumentType> getAll() {
        List<TipoDocumento> tipoDocumentos = (List<TipoDocumento>) tipoDocumentoCrudRepository.findAll();
        return documentTypeMapper.toDocumentTypes(tipoDocumentos);
    }

    @Override
    public DocumentType save(DocumentType documentType) {
        TipoDocumento tipoDocumento = documentTypeMapper.toTipoDocumento(documentType);
        return documentTypeMapper.toDocumentType(tipoDocumentoCrudRepository.save(tipoDocumento));
    }

    @Override
    public void delete(int documentTypeTypeId) {
        tipoDocumentoCrudRepository.deleteById(documentTypeTypeId);
    }

    @Override
    public Optional<DocumentType> getById(int id) {
        return tipoDocumentoCrudRepository.findById(id).map(i -> documentTypeMapper.toDocumentType(i));
    }
}