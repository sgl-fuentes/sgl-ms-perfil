package com.sgl.ms.perfil.persistence.crud.catalog;

import com.sgl.ms.perfil.persistence.entity.catalog.maestro.TipoDocumento;
import org.springframework.data.repository.CrudRepository;

public interface TipoDocumentoCrudRepository extends CrudRepository<TipoDocumento, Integer> {

}
