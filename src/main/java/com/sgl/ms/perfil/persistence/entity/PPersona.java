package com.sgl.ms.perfil.persistence.entity;

import com.sgl.ms.perfil.persistence.entity.catalog.maestro.*;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "persona")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PPersona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "usuario_creacion_id")
    private Integer usuarioCreacionId;
    @Column(name = "fecha_creacion")
    private LocalDateTime fechaCreacion;
    @Column(name = "usuario_actualizacion_id")
    private Integer usuarioActualizacionId;
    @Column(name = "fecha_actualizacion")
    private LocalDateTime fechaActualizacion;
    @Column(name = "tipo_documento_identidad_id")
    private Integer tipoDocumentoIdentidadId;
    @Column(name = "documento_identidad")
    private String documentoIdentidad;
    @Column(name = "apellido_paterno")
    private String apellidoPaterno;
    @Column(name = "apellido_materno")
    private String apellidoMaterno;
    @Column(name = "primero_nombre")
    private String primeroNombre;
    @Column(name = "segundo_nombre")
    private String segundoNombre;
    @Column(name = "email")
    private String email;
    @Column(name = "telefono_celular")
    private String telefonoCelular;
    @Column(name = "cargo_id")
    private Integer cargoId;
    @Column(name = "agrupacion_id")
    private Integer agrupacionId;
    @Column(name = "fecha_ingreso")
    private LocalDateTime fechaIngreso;
    @Column(name = "region_id")
    private Integer regionId;
    @Column(name = "estado_id")
    private Integer estadoId;
    @Column(name = "centro_costo_id")
    private Integer centroCostoId;
    @Column(name = "sueldo")
    private Double sueldo;
    @Column(name = "tipo_persona_id")
    private Integer tipoPersonaId;
    @Column(name = "codigo")
    private String codigo;
    @ManyToOne
    @JoinColumn(name = "tipo_documento_identidad_id", referencedColumnName = "id", insertable = false, updatable = false)
    TipoDocumentoIdentidad tipoDocumentoIdentidad;
    @ManyToOne
    @JoinColumn(name = "cargo_id", referencedColumnName = "id", insertable = false, updatable = false)
    Cargo cargo;
    @ManyToOne
    @JoinColumn(name = "agrupacion_id", referencedColumnName = "id", insertable = false, updatable = false)
    Agrupacion agrupacion;
    @ManyToOne
    @JoinColumn(name = "centro_costo_id", referencedColumnName = "id", insertable = false, updatable = false)
    CentroCosto centroCosto;
    @ManyToOne
    @JoinColumn(name = "region_id", referencedColumnName = "id", insertable = false, updatable = false)
    Region region;
    @ManyToOne
    @JoinColumn(name = "estado_id", referencedColumnName = "id", insertable = false, updatable = false)
    Estado estado;
    @ManyToOne
    @JoinColumn(name = "tipo_persona_id", referencedColumnName = "id", insertable = false, updatable = false)
    TipoPersona tipoPersona;
    @OneToMany(mappedBy = "persona")
    private List<PersonaDetalle> personaDetalles;
}
