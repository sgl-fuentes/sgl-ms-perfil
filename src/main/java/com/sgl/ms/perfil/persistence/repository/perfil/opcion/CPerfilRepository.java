package com.sgl.ms.perfil.persistence.repository.perfil.opcion;

import com.sgl.ms.perfil.domain.dto.profile.option.CProfile;
import com.sgl.ms.perfil.domain.repository.profile.option.CProfileRepository;
import com.sgl.ms.perfil.persistence.crud.perfil.custom.CustomPerfil;
import com.sgl.ms.perfil.persistence.crud.perfil.custom.CustomPerfilCrudRepository;
import com.sgl.ms.perfil.persistence.crud.perfil.opcion.CPerfilCrudRepository;
import com.sgl.ms.perfil.persistence.entity.perfil.opcion.CPerfil;
import com.sgl.ms.perfil.persistence.mapper.perfil.opcion.CProfileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class CPerfilRepository implements CProfileRepository {
    @Autowired
    private CPerfilCrudRepository cPerfilCrudRepository;
    @Autowired
    private CustomPerfilCrudRepository customPerfilCrudRepository;
    @Autowired
    private CProfileMapper cProfileMapper;

    @Override
    public List<CProfile> getAll() {
        List<CPerfil> cPerfiles = (List<CPerfil>) cPerfilCrudRepository.findAll();
        return cProfileMapper.toCProfiles(cPerfiles);
    }

    @Override
    public List<CProfile> findByFirstLevel() {
        List<CProfile> cProfiles = new ArrayList<>();
        CPerfil cPerfil = cPerfilCrudRepository.findById(1).get();
        cProfiles.add(cProfileMapper.toCProfile(cPerfil));
        return cProfiles;
    }

    @Override
    public List<CustomPerfil> getCustomPerfil() {
        return customPerfilCrudRepository.getCustomPerfil();
    }

}
