package com.sgl.ms.perfil.persistence.repository.perfil.control;

import com.sgl.ms.perfil.domain.dto.profile.control.CControl;
import com.sgl.ms.perfil.persistence.crud.perfil.control.ControlCrudRepository;
import com.sgl.ms.perfil.persistence.entity.perfil.control.Control;
import com.sgl.ms.perfil.persistence.mapper.perfil.control.ControlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ControlRepository implements com.sgl.ms.perfil.domain.repository.profile.control.ControlRepository {
    @Autowired
    private ControlCrudRepository controlCrudRepository;
    @Autowired
    private ControlMapper controlMapper;

    @Override
    public List<CControl> getAll() {
        List<Control> controles = (List<Control>) controlCrudRepository.findAll();
        return controlMapper.toCControls(controles);
    }

    @Override
    public Optional<List<CControl>> getByOptionId(int id) {
        List<Control> controles = (List<Control>) controlCrudRepository.findByOpcionId(id);
        return Optional.of(controlMapper.toCControls(controles));
    }
}