package com.sgl.ms.perfil.persistence.entity.perfil.opcion;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
@Entity
@Table(name = "opcion_perfil")
public class CPerfilBarra {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "usuario_creacion_id")
    private Integer usuarioCreacionId;

    @Column(name = "fecha_creacion")
    private LocalDateTime fechaCreacion;

    @Column(name = "usuario_actualizacion_id")
    private Integer usuarioActualizacionId;

    @Column(name = "fecha_actualizacion")
    private LocalDateTime fechaActualizacion;

    @Column(name = "opcion_id")
    private Integer barraId;

    @Column(name = "perfil_id")
    private Integer perfilId;

    @Column(name = "selected")
    private Boolean selected;

    @Column(name = "indeterminate")
    private Boolean indeterminate;

    @ManyToOne
    @JoinColumn(name = "perfil_id", referencedColumnName = "id", insertable = false, updatable = false)
    private CPerfil cPerfil;
    @ManyToOne
    @JoinColumn(name = "opcion_id", referencedColumnName = "id", insertable = false, updatable = false)
    private CBarra cBarra;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUsuarioCreacionId() {
        return usuarioCreacionId;
    }

    public void setUsuarioCreacionId(Integer usuarioCreacionId) {
        this.usuarioCreacionId = usuarioCreacionId;
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getUsuarioActualizacionId() {
        return usuarioActualizacionId;
    }

    public void setUsuarioActualizacionId(Integer usuarioActualizacionId) {
        this.usuarioActualizacionId = usuarioActualizacionId;
    }

    public LocalDateTime getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(LocalDateTime fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Integer getBarraId() {
        return barraId;
    }

    public void setBarraId(Integer barraId) {
        this.barraId = barraId;
    }

    public Integer getPerfilId() {
        return perfilId;
    }

    public void setPerfilId(Integer perfilId) {
        this.perfilId = perfilId;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Boolean getIndeterminate() {
        return indeterminate;
    }

    public void setIndeterminate(Boolean indeterminate) {
        this.indeterminate = indeterminate;
    }

    public CPerfil getcPerfil() {
        return cPerfil;
    }

    public void setcPerfil(CPerfil cPerfil) {
        this.cPerfil = cPerfil;
    }

    public CBarra getcBarra() {
        return cBarra;
    }

    public void setcBarra(CBarra cBarra) {
        this.cBarra = cBarra;
    }
}
