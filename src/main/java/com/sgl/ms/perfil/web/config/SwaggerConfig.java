package com.sgl.ms.perfil.web.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.apache.coyote.Request;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.beans.BeanProperty;


@Configuration
public class SwaggerConfig {
    @Bean
    GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder()
                .group("sgl-apis")
                .pathsToMatch("/**")
                .build();
    }

    OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("SGL API Documentation").version("v1.0.0"))
                .components(
                        new Components()
                                .addSecuritySchemes("pass123", new SecurityScheme()
                                        .type(SecurityScheme.Type.HTTP)
                                        .scheme("bearer")
                                        .bearerFormat("JWT")));
    }
}
