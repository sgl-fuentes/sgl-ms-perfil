package com.sgl.ms.perfil.web.controller.security;

import com.sgl.ms.perfil.domain.dto.User;
import com.sgl.ms.perfil.domain.dto.security.LoginDto;
import com.sgl.ms.perfil.domain.dto.security.Token;
import com.sgl.ms.perfil.domain.service.UserService;
import com.sgl.ms.perfil.web.config.JwtUtil;
import com.sgl.ms.perfil.web.controller.perfil.opcion.CProfileMenuController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/auth")
public class AuthController {
    Logger logger = LoggerFactory.getLogger(AuthController.class);
    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager, JwtUtil jwtUtil) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
    }

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public ResponseEntity<Token> login(@RequestBody  LoginDto loginDto){
        try{
            Token token = new Token();
            UsernamePasswordAuthenticationToken login = new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword());
            Authentication authentication = this.authenticationManager.authenticate(login);
            String jwt = this.jwtUtil.create(loginDto.getUsername());
            token.setToken(jwt);
            return ResponseEntity.ok()
                    .header(HttpHeaders.AUTHORIZATION, jwt)
                    .body(token);
        }
        catch(Exception ex){
            logger.error("ERROR Start");
            logger.error("Error Message : " + ex.getMessage());
            logger.error("ERROR End");
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/get/passwordByCode")
    public ResponseEntity<User> getPasswordByCode(@RequestBody User user){
        String password = userService.getByCode(user.getCode()).get().getPassword();
        User newUser = new User();
        newUser.setCode(user.getCode());
        newUser.setPassword(password);
        if(newUser != null){
            return new ResponseEntity<>(newUser, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
}
