package com.sgl.ms.perfil.web.controller;

import com.sgl.ms.perfil.domain.dto.User;
import com.sgl.ms.perfil.domain.dto.response.UserProfileResponse;
import com.sgl.ms.perfil.domain.service.UserProfileService;
import com.sgl.ms.perfil.domain.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserProfileService userProfileService;

    //@CrossOrigin
    @GetMapping("/all")
    public ResponseEntity<List<User>> getAll() {
        return new ResponseEntity<>(userService.getAll(), HttpStatus.OK);
    }
    //@CrossOrigin
    //@CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getUserWithoutProfile")
    public ResponseEntity<List<User>> getUserWithoutProfile() {
        if (userService.getUserWithoutProfile().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else
        {
            return new ResponseEntity<>(userService.getUserWithoutProfile(), HttpStatus.OK);
        }
    }

    /*
    @GetMapping("/code/{code}")
    public ResponseEntity<User> getByCode(@PathVariable("code") String code) {
        return userService.getByCode(code)
                .map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    */

    //@CrossOrigin
    @GetMapping("/get/{id}")
    public ResponseEntity<User> getById(@PathVariable("id") int id){
        return userService.getById(id)
                .map(usr -> new ResponseEntity<>(usr, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    @PostMapping("/get/userpassword")
    public ResponseEntity<User> getByCodeAndPassword(@RequestBody User user){
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        String password = userService.getByCode(user.getCode()).get().getPassword();
        boolean isValid = bcrypt.matches(user.getPassword(), password);
        if(isValid){
            return userService.getByCodeAndPassword(user.getCode(), password)
                    .map(usr -> new ResponseEntity<>(usr, HttpStatus.OK))
                    .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        }
        else{
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/get/passwordByCode")
    public ResponseEntity<User> getPasswordByCode(@RequestBody User user){
        String password = userService.getByCode(user.getCode()).get().getPassword();
        User newUser = new User();
        newUser.setCode(user.getCode());
        newUser.setPassword(password);
        if(newUser != null){
            return new ResponseEntity<>(newUser, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
    //@CrossOrigin
    @PostMapping("/update")
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        User updatedUser = userService.getById(user.getId()).get();
        if(user.getPassword() != null) {
            String password = bcrypt.encode(user.getPassword());
            user.setPassword(password);
        }
        else{
            user.setPassword(updatedUser.getPassword());
        }
        return new ResponseEntity<>(userService.update(user), HttpStatus.CREATED);
    }

    //@CrossOrigin
    @PostMapping("/save")
    public ResponseEntity<User> save(@RequestBody User user) {
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        String password = bcrypt.encode(user.getPassword());
        user.setPassword(password);
        return new ResponseEntity<>(userService.save(user), HttpStatus.CREATED);
    }

    //@CrossOrigin
    @GetMapping("/getProfileByUserId/{idUsuario}")
    public ResponseEntity<List<UserProfileResponse>> findByUsuarioId(@PathVariable("idUsuario") int idUsuario) {
        if (userProfileService.findByUsuarioId(idUsuario).isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else
        {
            return new ResponseEntity<>(userProfileService.findByUsuarioId(idUsuario), HttpStatus.OK);
        }
    }
}
