package com.sgl.ms.perfil.web.controller.perfil.control;

import com.sgl.ms.perfil.domain.dto.profile.control.ControlType;
import com.sgl.ms.perfil.domain.service.profile.control.ControlTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/controlType")
public class ControlTypeController {
    @Autowired
    private ControlTypeService controlTypeService;

    //@CrossOrigin
    @GetMapping("/all")
    public ResponseEntity<List<ControlType>> getAll() {
        return new ResponseEntity<>(controlTypeService.getAll(), HttpStatus.OK);
    }
    //@CrossOrigin
    @GetMapping("/getById/{id}")
    public ResponseEntity<ControlType> getProduct(@PathVariable("id") int id) {
        return controlTypeService.getControlTypeById(id)
                .map(controlType -> new ResponseEntity<>(controlType, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    //@CrossOrigin
    @PostMapping("/save")
    public ResponseEntity<ControlType> save(@RequestBody ControlType controlType) {
        return new ResponseEntity<>(controlTypeService.save(controlType), HttpStatus.CREATED);
    }
}
