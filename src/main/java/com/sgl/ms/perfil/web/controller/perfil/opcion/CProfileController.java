package com.sgl.ms.perfil.web.controller.perfil.opcion;

import com.sgl.ms.perfil.domain.dto.profile.option.CProfile;
import com.sgl.ms.perfil.domain.service.profile.option.CProfileService;
import com.sgl.ms.perfil.persistence.crud.perfil.custom.CustomPerfil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/profile/opcion")
public class CProfileController {
    @Autowired
    private CProfileService cProfileService;

    @CrossOrigin
    @GetMapping("/all")
    public ResponseEntity<List<CProfile>> getAll() {
        return new ResponseEntity<>(cProfileService.getAll(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/findByFirstLevel")
    public ResponseEntity<List<CustomPerfil>> getCustomPerfil() {
        return new ResponseEntity<>(cProfileService.getCustomPerfil(), HttpStatus.OK);
    }
}
