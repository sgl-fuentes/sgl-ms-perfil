package com.sgl.ms.perfil.web.controller;

import com.sgl.ms.perfil.domain.dto.*;
import com.sgl.ms.perfil.domain.dto.request.ItemRequest;
import com.sgl.ms.perfil.domain.dto.request.MenuRequest;
import com.sgl.ms.perfil.domain.dto.request.OptionRequest;
import com.sgl.ms.perfil.domain.dto.response.MenuResponse;
import com.sgl.ms.perfil.domain.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/menus")
public class MenuController {
    @Autowired
    private MenuService menuService;
    /*
    @GetMapping("/all")
    public ResponseEntity<List<Menu>> getAll() {
        return new ResponseEntity<>(menuService.getAll(), HttpStatus.OK);
    }
    @GetMapping("/allCollapsable")
    public ResponseEntity<List<Menu>> getAllCollapsable() {
        return new ResponseEntity<>(menuService.getAllCollapsable(), HttpStatus.OK);
    }
    @GetMapping("/allTreeMenu")
    public ResponseEntity<List<Menu>> getAllMenu() {
        return new ResponseEntity<>(menuService.findAllMenu("collapsable", "item"), HttpStatus.OK);
    }
    */
    /*
    @GetMapping("/allOptionMenu")
    public ResponseEntity<List<MenuResponse>> getAllTreeMenu() {
        return new ResponseEntity<>(menuService.findAllTreeMenu(1), HttpStatus.OK);
    }
    */
    //@CrossOrigin
    @GetMapping("/allLevelMenu")
    public ResponseEntity<List<MenuResponse>> getAllByNivel() {
        return new ResponseEntity<>(menuService.findByLevel(1), HttpStatus.OK);
    }
    //@CrossOrigin
    @PostMapping("/saveMenu")
    public ResponseEntity<Menu> saveMenu(@RequestBody MenuRequest menuRequest) {
        return new ResponseEntity<>(menuService.save(menuRequest), HttpStatus.CREATED);
    }
    //@CrossOrigin
    @PostMapping("/saveOption")
    public ResponseEntity<Option> saveOption(@RequestBody OptionRequest optionRequest) {
        return new ResponseEntity<>(menuService.saveOption(optionRequest), HttpStatus.CREATED);
    }

    //@CrossOrigin
    @PostMapping("/saveItem")
    public ResponseEntity<Item> saveItem(@RequestBody ItemRequest itemRequest) {
        return new ResponseEntity<>(menuService.saveItem(itemRequest), HttpStatus.CREATED);
    }
    //@CrossOrigin
    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int menuId) {
        if (menuService.delete(menuId)) {
            return new ResponseEntity(HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
