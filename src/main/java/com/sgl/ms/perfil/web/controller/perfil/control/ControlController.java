package com.sgl.ms.perfil.web.controller.perfil.control;

import com.sgl.ms.perfil.domain.dto.profile.control.CControl;
import com.sgl.ms.perfil.domain.dto.profile.control.CControlProfile;
import com.sgl.ms.perfil.domain.dto.profile.control.ControlType;
import com.sgl.ms.perfil.domain.dto.response.CControlProfileResponse;
import com.sgl.ms.perfil.domain.service.profile.control.ControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/control")
public class ControlController {
    @Autowired
    private ControlService controlService;

    //@CrossOrigin
    @GetMapping("/all")
    public ResponseEntity<List<CControl>> getAll() {
        return new ResponseEntity<>(controlService.getAll(), HttpStatus.OK);
    }

    //@CrossOrigin
    @GetMapping("/getAllByProfileId/{id}")
    public ResponseEntity<List<CControlProfile>> getAllByProfileId(@PathVariable("id") int id) {
        if(controlService.getCControlProfileByProfileId(id) != null){
            return new ResponseEntity<>(controlService.getCControlProfileByProfileId(id), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(Collections.EMPTY_LIST, HttpStatus.NOT_FOUND);
        }
    }

    //@CrossOrigin
    @PostMapping("/saveAll")
    public ResponseEntity<List<CControlProfileResponse>> saveAll(@RequestBody List<CControlProfile> cControlProfiles) {
        return new ResponseEntity<>(controlService.saveAll(cControlProfiles), HttpStatus.CREATED);
    }
}
