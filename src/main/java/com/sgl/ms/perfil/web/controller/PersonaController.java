package com.sgl.ms.perfil.web.controller;

import com.sgl.ms.perfil.domain.dto.Person;
import com.sgl.ms.perfil.domain.dto.PersonDetail;
import com.sgl.ms.perfil.domain.dto.Product;
import com.sgl.ms.perfil.domain.service.PersonDetailService;
import com.sgl.ms.perfil.domain.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/persons")
public class PersonaController {
    @Autowired
    private PersonService personService;
    @Autowired
    private PersonDetailService personDetailService;

    //@CrossOrigin
    @GetMapping("/all")
    public ResponseEntity<List<Person>> getAll() {
        if (personService.getAll().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<>(personService.getAll(), HttpStatus.OK);
        }
    }

    //@CrossOrigin
    @PostMapping("/save")
    public ResponseEntity<Person> save(@RequestBody Person person) {
        List<Person> persons = personService.findByTipoDocumentoIdentidadIdAndDocumentoIdentidad(person.getIdentityDocumentTypeId(), person.getIdentityDocument());
        if(person.getId() == null) {
            for(Person p : persons) {
                if(p.getIdentityDocumentTypeId().equals(person.getIdentityDocumentTypeId()) && p.getIdentityDocument().equals(person.getIdentityDocument())){
                    return new ResponseEntity<>(HttpStatus.FOUND);
                }
            }
            return new ResponseEntity<>(personService.save(person), HttpStatus.CREATED);
        }
        else {
            if (persons.size() == 0) {
                return new ResponseEntity<>(personService.save(person), HttpStatus.CREATED);
            }
            else {
                for(Person p : persons) {
                    if(p.getIdentityDocumentTypeId().equals(person.getIdentityDocumentTypeId()) && p.getIdentityDocument().equals(person.getIdentityDocument()) && p.getId().equals(person.getId())){
                        return new ResponseEntity<>(personService.save(person), HttpStatus.CREATED);
                    }
                }
                return new ResponseEntity<>(HttpStatus.FOUND);
            }
        }
    }

    //@CrossOrigin
    @GetMapping("/get/{id}")
    public ResponseEntity<Person> getPerson(@PathVariable("id") int id) {
        return personService.getPersonById(id)
                .map(person -> new ResponseEntity<>(person, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    //@CrossOrigin
    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int personId) {
        if (personService.delete(personId)) {
            return new ResponseEntity(HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    //@CrossOrigin
    @GetMapping("/detail/get/{personId}")
    public ResponseEntity<List<PersonDetail>> getPersonDetail(@PathVariable("personId") int id) {
        return personDetailService.getPersonDetailByPersonaId(id)
                .map(personDetail -> new ResponseEntity<>(personDetail, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    //@CrossOrigin
    @PostMapping("/detail/saveAll")
    public ResponseEntity<List<PersonDetail>> save(@RequestBody List<PersonDetail> personDetails) {
        return new ResponseEntity<>(personDetailService.saveAll(personDetails), HttpStatus.CREATED);
    }

    @DeleteMapping("/detail/delete/{id}")
    public ResponseEntity deleteDetail(@PathVariable("id") int personDetailId) {
        if (personDetailService.delete(personDetailId)) {
            return new ResponseEntity(HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
