package com.sgl.ms.perfil.web.controller;

import com.sgl.ms.perfil.domain.dto.Profile;
import com.sgl.ms.perfil.domain.dto.ProfileUser;
import com.sgl.ms.perfil.domain.dto.UserProfile;
import com.sgl.ms.perfil.domain.dto.response.UserByProfileResponse;
import com.sgl.ms.perfil.domain.dto.response.UserProfileResponse;
import com.sgl.ms.perfil.domain.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/profiles")
public class ProfileController {
    @Autowired
    private ProfileService profileService;

    //@CrossOrigin
    @GetMapping("/all")
    public ResponseEntity<List<Profile>> getAll() {
        return new ResponseEntity<>(profileService.getAll(), HttpStatus.OK);
    }
    /*
    @CrossOrigin
    @GetMapping("/description/{description}")
    public ResponseEntity<List<Profile>> getByDescription(@PathVariable("description") String description) {
        return new ResponseEntity<>(profileService.getByDescription(description), HttpStatus.OK);
    }
    */

    //@CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<Profile> getById(@PathVariable("id") int idProfile) {
        return profileService.getById(idProfile)
                .map(profile -> new ResponseEntity<>(profile, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    //@CrossOrigin
    @PostMapping("/save")
    public ResponseEntity<Profile> save(@RequestBody Profile profile) {
        return new ResponseEntity<>(profileService.save(profile), HttpStatus.CREATED);
    }

    /*
    @CrossOrigin
    @PostMapping("/save/user")
    public ResponseEntity<ProfileUser> saveProfileUser(@RequestBody ProfileUser profileUser) {
        return new ResponseEntity<>(profileService.saveProfileUser(profileUser), HttpStatus.CREATED);
    }
    */

    //@CrossOrigin
    @PostMapping("/save/listUser")
    public ResponseEntity<List<ProfileUser>> saveListProfileUser(@RequestBody List<ProfileUser> profileUsers) {
        return new ResponseEntity<>(profileService.saveListProfileUser(profileUsers), HttpStatus.CREATED);
    }

    //@CrossOrigin
    @GetMapping("/getUserByProfileId/{id}")
    public ResponseEntity<UserByProfileResponse> getUserByProfileId(@PathVariable("id") int idProfile) {
        return profileService.getUserByProfileId(idProfile)
                .map(userByProfile -> new ResponseEntity<>(userByProfile, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    //@CrossOrigin
    @DeleteMapping("/delete/userprofile/{userId}/{profileId}")
    public ResponseEntity delete(@PathVariable("userId") int userId, @PathVariable("profileId") int profileId) {
        if (profileService.deleteById(userId, profileId)) {
            return new ResponseEntity(HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
