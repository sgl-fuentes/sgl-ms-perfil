package com.sgl.ms.perfil.web.controller.table;

import com.sgl.ms.perfil.domain.dto.table.SubTable;
import com.sgl.ms.perfil.domain.dto.table.Table;
import com.sgl.ms.perfil.domain.service.table.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/tables")
public class TableController {
    @Autowired
    private TableService tableService;
    //@CrossOrigin
    @GetMapping("/core/{id}")
    public ResponseEntity<List<Table>> getAllCoreTable(@PathVariable("id") int optionId) {
        List<Table> listas = tableService.getTableByFatherIsNullAndOptionId(optionId).get().stream().sorted(Comparator.comparingInt(Table::getOrder)).toList();
        listas.forEach(i -> {
            i.setChildren(i.getChildren().stream().sorted(Comparator.comparingInt(SubTable::getOrder)).toList());
        });
        return new ResponseEntity<>(listas, HttpStatus.OK);
    }
}