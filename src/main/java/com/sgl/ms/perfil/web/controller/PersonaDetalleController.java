package com.sgl.ms.perfil.web.controller;

import com.sgl.ms.perfil.domain.dto.Person;
import com.sgl.ms.perfil.domain.dto.Product;
import com.sgl.ms.perfil.domain.dto.request.PersonDetailRequest;
import com.sgl.ms.perfil.domain.service.PersonDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/person/detail")
public class PersonaDetalleController {
    @Autowired
    private PersonDetailService personDetailService;

    @CrossOrigin
    @PostMapping("/save")
    public ResponseEntity<PersonDetailRequest> save(@RequestBody PersonDetailRequest personDetailRequest) {
        return new ResponseEntity<>(personDetailService.save(personDetailRequest), HttpStatus.CREATED);
    }

    @CrossOrigin
    @PostMapping("/saveAll")
    public ResponseEntity<List<PersonDetailRequest>> save(@RequestBody List<PersonDetailRequest> personDetailRequests) {
        return new ResponseEntity<>(personDetailService.saveAll(personDetailRequests), HttpStatus.CREATED);
    }

    @CrossOrigin
    @GetMapping("/all")
    public ResponseEntity<List<PersonDetailRequest>> getAll() {
        if (personDetailService.getAll().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<>(personDetailService.getAll(), HttpStatus.OK);
        }
    }
    @CrossOrigin
    @GetMapping("/get/{personId}")
    public ResponseEntity<List<PersonDetailRequest>> getPersonDetailByPersonId(@PathVariable("personId") int personId) {
        if (personDetailService.getPersonDetailByPersonaId(personId) != null) {
            return new ResponseEntity<>(personDetailService.getPersonDetailByPersonaId(personId), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @CrossOrigin
    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int personDetailId) {
        if (personDetailService.delete(personDetailId)) {
            return new ResponseEntity(HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
