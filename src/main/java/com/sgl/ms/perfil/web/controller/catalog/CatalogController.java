package com.sgl.ms.perfil.web.controller.catalog;

import com.sgl.ms.perfil.domain.dto.Product;
import com.sgl.ms.perfil.domain.dto.catalog.Catalog;
import com.sgl.ms.perfil.domain.dto.request.OptionRequest;
import com.sgl.ms.perfil.domain.service.catalog.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/catalogs")
public class CatalogController {
    @Autowired
    private CatalogService catalogService;

    //@CrossOrigin
    @GetMapping("/{catalog}")
    public ResponseEntity<List<?>> getAllCatalog(@PathVariable("catalog") String catalog) {
        if (catalogService.getAllCatalog(catalog).isEmpty() || catalogService.getAllCatalog(catalog) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<>(catalogService.getAllCatalog(catalog), HttpStatus.OK);
        }
    }
    @PostMapping("/save/{catalog}")
    public ResponseEntity<?> saveCatalog(@PathVariable("catalog") String entity, @RequestBody Catalog catalog) {
        switch(entity) {
            case "Group":
                return new ResponseEntity<>(catalogService.saveGroup(catalog), HttpStatus.CREATED);
            case "IdentityDocumentType":
                return new ResponseEntity<>(catalogService.saveIdentityDocumentType(catalog), HttpStatus.CREATED);
            case "WorkPosition":
                return new ResponseEntity<>(catalogService.saveWorkPosition(catalog), HttpStatus.CREATED);
            case "DocumentType":
                return new ResponseEntity<>(catalogService.saveDocumentType(catalog), HttpStatus.CREATED);
            default:
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{catalog}/id/{id}")
    public ResponseEntity<?> getCatalogId(@PathVariable("catalog") String entity, @PathVariable("id") int id) {
        switch(entity) {
            case "Group":
                return catalogService.getGroupId(id)
                        .map(i -> new ResponseEntity<>(i, HttpStatus.OK))
                        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
            case "IdentityDocumentType":
                return catalogService.getIdentityDocumentTypeId(id)
                        .map(i -> new ResponseEntity<>(i, HttpStatus.OK))
                        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
            case "WorkPosition":
                return catalogService.getWorkPositionId(id)
                        .map(i -> new ResponseEntity<>(i, HttpStatus.OK))
                        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
            case "DocumentType":
                return catalogService.getDocumentTypeId(id)
                        .map(i -> new ResponseEntity<>(i, HttpStatus.OK))
                        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
            default:
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/delete/{catalog}/{id}")
    public ResponseEntity delete(@PathVariable("catalog") String entity, @PathVariable("id") int id) {
        switch(entity) {
            case "Group":
                if (catalogService.deleteGroup(id)) {
                    return new ResponseEntity(HttpStatus.OK);
                }
                else
                {
                    return new ResponseEntity(HttpStatus.NOT_FOUND);
                }
            case "IdentityDocumentType":
                if (catalogService.deleteIdentityDocumentType(id)) {
                    return new ResponseEntity(HttpStatus.OK);
                }
                else
                {
                    return new ResponseEntity(HttpStatus.NOT_FOUND);
                }
            case "WorkPosition":
                if (catalogService.deleteWorkPosition(id)) {
                    return new ResponseEntity(HttpStatus.OK);
                }
                else
                {
                    return new ResponseEntity(HttpStatus.NOT_FOUND);
                }
            case "DocumentType":
                if (catalogService.deleteDocumentType(id)) {
                    return new ResponseEntity(HttpStatus.OK);
                }
                else
                {
                    return new ResponseEntity(HttpStatus.NOT_FOUND);
                }
            default:
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
