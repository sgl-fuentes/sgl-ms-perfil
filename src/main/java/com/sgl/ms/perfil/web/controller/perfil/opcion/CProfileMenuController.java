package com.sgl.ms.perfil.web.controller.perfil.opcion;

import com.sgl.ms.perfil.domain.dto.profile.control.CControl;
import com.sgl.ms.perfil.domain.dto.profile.control.CControlProfile;
import com.sgl.ms.perfil.domain.dto.response.MenuResponse;
import com.sgl.ms.perfil.domain.dto.response.OptionResponse;
import com.sgl.ms.perfil.domain.dto.response.UserProfileResponse;
import com.sgl.ms.perfil.domain.service.UserProfileService;
import com.sgl.ms.perfil.domain.service.profile.control.ControlService;
import com.sgl.ms.perfil.domain.service.profile.option.CProfileMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("/profile/menu")
public class CProfileMenuController {
    Logger logger = LoggerFactory.getLogger(CProfileMenuController.class);
    @Autowired
    private CProfileMenuService cProfileMenuService;
    @Autowired
    private UserProfileService userProfileService;
    @Autowired
    private ControlService controlService;
    @CrossOrigin
    @GetMapping("/getByProfileId/{idProfile}")
    public ResponseEntity<List<MenuResponse>> getByProfileId(@PathVariable("idProfile") int idProfile) {
        List<MenuResponse> menuSortedResponse = new ArrayList<>();
        if (!cProfileMenuService.getByProfileId(idProfile).isEmpty()) {
            menuSortedResponse = cProfileMenuService.getByProfileId(idProfile)
                    .stream().sorted(Comparator.comparingInt(MenuResponse::getOrder)).toList();
            menuSortedResponse.forEach(o -> {
                o.setChildren(
                        o.getChildren().stream().sorted(Comparator.comparingInt(OptionResponse::getOrder)).toList()
                );
            });
            return new ResponseEntity<>(menuSortedResponse, HttpStatus.OK);
        }
        else{
            return new ResponseEntity(new ArrayList<>(), HttpStatus.NOT_FOUND);
        }
    }

    @CrossOrigin
    @PostMapping("/saveByProfile/{idProfile}")
    public ResponseEntity<List<MenuResponse>> save(@PathVariable("idProfile") int idProfile, @RequestBody List<MenuResponse> menuResponses) {
        //logger.trace("A TRACE Message");
        //logger.debug("A DEBUG Message");
        //logger.info("An INFO Message");
        //logger.warn("A WARN Message");

        try{
            Integer size = this.cProfileMenuService.getByProfileId(idProfile).size();
            if(size > 0){
                this.cProfileMenuService.deleteByProfileId(idProfile);
            }
            List<MenuResponse> menuReponse = cProfileMenuService.save(idProfile, menuResponses);

            return new ResponseEntity<>(menuReponse, HttpStatus.OK);
        }
        catch(Exception ex){
            logger.error("ERROR Start");
            logger.error("Error Message : " + ex.getMessage());
            logger.error("ERROR End");
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @GetMapping("/getMenuByUserId/{userId}")
    public ResponseEntity<List<MenuResponse>> save(@PathVariable("userId") int userId) {
        List<UserProfileResponse> userProfileResponse = userProfileService.findByUsuarioId(userId);
        List<MenuResponse> menuResponse = new ArrayList<>();
        for(UserProfileResponse up : userProfileResponse) {
            for (MenuResponse mr : cProfileMenuService.getByProfileId(up.getProfileId())) {
                List<OptionResponse> newChildrens = new ArrayList<>();
                for (OptionResponse or : mr.getChildren()) {
                    if (or.getSelected() == null) {
                        or.setSelected(false);
                    }
                    if (or.getIndeterminate() == null) {
                        or.setIndeterminate(false);
                    }
                    /*
                    if (or.getSelected() || or.getIndeterminate()) {
                        newChildrens.add(or);
                    }
                    */
                    if (or.getSelected()) {
                        newChildrens.add(or);
                    }
                }
                mr.setChildren(newChildrens);
                menuResponse.add(mr);
            }
        }

        List<MenuResponse> menuResponseResult = new ArrayList<>();
        for(MenuResponse response : menuResponse) {
            if (response.getSelected() == null) {
                response.setSelected(false);
            }
            if (response.getIndeterminate() == null) {
                response.setIndeterminate(false);
            }
            /*
            if (response.getSelected() || response.getIndeterminate()) {
                menuResponseResult.add(response);
            }
            */
            if (response.getSelected()) {
                menuResponseResult.add(response);
            }
            else {
                if (!response.getSelected() && response.getChildren().size() > 0) {
                    menuResponseResult.add(response);
                }
            }
        }

        if(menuResponse.isEmpty()) {
            return new ResponseEntity(new ArrayList<>(), HttpStatus.NOT_FOUND);
        }
        else {
            List<MenuResponse> menuSortedResponse = menuResponseResult.stream().sorted(Comparator.comparingInt(MenuResponse::getOrder)).toList();
            menuSortedResponse.forEach(c -> {
                c.setChildren(
                        c.getChildren().stream().sorted(Comparator.comparingInt(OptionResponse::getOrder)).toList()
                );
            });
            return new ResponseEntity<>(menuSortedResponse, HttpStatus.OK);
        }
    }
}